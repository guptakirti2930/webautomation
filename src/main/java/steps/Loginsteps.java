package steps;
//steps
//method calling of screen and gherkin syntax

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import screens.LoginScreen;

import com.cars24.library.WebSessionManager;
import com.cars24.library.WebLibrary;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;


/*author
*
* Kirti gupta
* */

public class Loginsteps extends WebSessionManager {
    private WebDriver driver = WebSessionManager.getInstance().driver;
    private LoginScreen loginscreen = new LoginScreen(driver);
    private WebLibrary webLibrary = new WebLibrary(driver);

    @Given("^User navigates to login page$")
    public void loginpage() {
        Reporter.log("verify login screen");
        verifyLoginScreenElements();

    }

    @When("^User enters (.+) and (.+)$")
    public void enterValidCredentials(String userName, String userPaasword) {
        webLibrary.enterText(loginscreen.getUsername(), userName);
        webLibrary.enterText(loginscreen.getPassword(), userPaasword);
        webLibrary.clickonWebElement(loginscreen.getSubmitButton());
    }

    @Then("^User is (.+) logged in$")
    public void verifyLogin(String state) {
        if (state.equalsIgnoreCase("successful")) {
            Reporter.log("User successfully loggedin", true);
            Assert.assertTrue(isLoginSucessful(),"successfully login");
        } else {
            Reporter.log("Unsuccessful logged in", true);
            Assert.assertTrue(unSuccessfullLogin(),"unsuccessful login");
        }
    }

    private void verifyLoginScreenElements() {
        webLibrary.waitforWebElement(loginscreen.getUsername(), Integer.parseInt(dataProp.getProperty("DEFAULT_OBJECT_WAIT_TIME")));
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(loginscreen.getUsername()),"gettingusername");
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(loginscreen.getPassword()),"gettingpassword");
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(loginscreen.getSubmitButton()),"clickon login button");
    }

    private boolean unSuccessfullLogin() {
        if (!(webLibrary.isElementPresentOnScreen(loginscreen.getSuccessfullyLoggedIn()))) {
            Reporter.log("User is not logged in", true);
            return true;
        } else {
            Reporter.log("User is already loggedin",true);
            return false;
        }
    }

    public boolean isLoginSucessful() {
        if (webLibrary.isElementPresentOnScreen(loginscreen.getSuccessfullyLoggedIn())) {
            Reporter.log("Sign up SuccessFull", true);
            createLoginFolder();
            return true;
        } else
            Reporter.log("user unable to loggedin",true);
        Assert.fail("Login Failure");
        return false;
    }

    private void createLoginFolder() {
        Reporter.log("Creating login folder", true);
        String loginState = "login = 1";
        loginState("login.txt", loginState);
    }

    private void loginState(String fileName, String loginState) {
        PrintWriter logFileWriter = null;
        try {
            FileUtils.forceMkdir(new File("loginFolder"));
            File logFile = new File("loginFolder/" + fileName);
            logFileWriter = new PrintWriter(logFile);
            logFileWriter.println(loginState);
            logFileWriter.flush();
        } catch (IOException e) {
            Reporter.log("Could not write log file\n" + ExceptionUtils.getStackTrace(e), true);
        } catch (Exception e) {
            Reporter.log("Exception while capturing device logs\n" + ExceptionUtils.getStackTrace(e),
                    true);
        } finally {
            if (logFileWriter != null) {
                logFileWriter.close();
            }
        }
    }
}


