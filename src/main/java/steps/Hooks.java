package steps;

import com.cars24.library.WebLibrary;
import com.cars24.library.WebSessionManager;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;


/**
 * @author kirti gupta
 */

public class Hooks {

    @Before
    public void setup() {
        try {


            WebSessionManager.getInstance().initiateWebDriverInstance(WebSessionManager.browsername);

        } catch (InterruptedException e) {
            Reporter.log("browser not found");
        }
    }

    @After
    public void teardown(Scenario scenario) {
        long start = System.currentTimeMillis();
        Reporter.log(
                "*********************************************Executing hooks********************************************", true);
        WebDriver driver = WebSessionManager.getInstance().driver;
        WebLibrary webLibrary = new WebLibrary(driver);
        if (scenario.isFailed() && driver != null) {

            try {
                String screenshotName = scenario.getName() + "-" + System.currentTimeMillis() + ".jpg";
                scenario.embed(webLibrary.createScreenshot(screenshotName), "image/png");
            } catch (Exception e) {
                Reporter.log("Could not capture/embed screenshot\n" + ExceptionUtils.getStackTrace(e),
                        true);
            }
        }


        driver.close();
        driver.quit();
        long end = System.currentTimeMillis();
        Reporter.log("****************Time taken to execute hook for device : " + "  --- " + (end - start), true);
        System.gc();

    }
}



