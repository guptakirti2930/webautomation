#Author - ishita
# Reviewer -

  #pre-requisite:
  # 1. URL : lms-qa23.ninja24.in
  # 2. User should be logged in into LMS.

@lms @create-lead
Feature: Create Lead Functionality
  As a user,
  I want to create a walkin lead

  Background:
    Given User is already logged in on LMS

  @lead-validation
  Scenario: User is able to verify error messages on create lead page
    Given User is on home menu
    When User clicks on create lead option from menu
    And User clicks on create new lead button without entering any data
    Then User should get validation error messages on every field

## Book appointment form has following fields - customer name, customer phone, customer email, make, model, year, variant, odometer,
## registration state, store city, branch name, appointment date, appointment time, source of awareness, reason of selling car

  @lms-sanity
  Scenario: User is able to create a walkin lead
    Given User is on create lead page
    When User fills book appointment form
      | TestLead | 9999999999 | test@gmail.com | buying new car |
    And User clicks on create new lead button
    Then User should successfully be able to create an appointment


