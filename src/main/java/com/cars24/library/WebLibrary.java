package com.cars24.library;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.testng.Reporter;
import java.io.File;
import java.io.FileInputStream;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

/**
 * @author kirtigupta
 */

public class WebLibrary {
    private WebDriver driver;

    public WebLibrary(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);
    }

    public void clickonWebElement(WebElement element) {

        try {
            waitforWebElement(element, 10);
            Reporter.log("clicking on element" + element, true);
            if (element != null) {
                element.click();
            } else {
                Assert.fail("Element is null i.e. element was not captured by PageFactory initiation");
            }

        } catch (ElementNotVisibleException | NoSuchElementException | StaleElementReferenceException e) {
            Assert.fail("Unable to click on element" + "\n" + ExceptionUtils.getStackTrace(e));


        }
    }


    public void enterText(WebElement element, String value) {
        Reporter.log("Entering Text" + element, true);
        if (element == null) {
            Assert.fail(String.valueOf(element) + "is null, i.e. element was not captured by PageFactory initialization");
            return;
        }
        clickonWebElement(element);
        try {
            element.sendKeys(value);
        } catch (Exception e) {
            System.out.print("exception caught" + e);
        }

    }

    public String fetchText(WebElement element) {
        String text = null;
        Reporter.log("Fetching text from element- " + element, true);
        if (element == null) {
            Assert.fail(String.valueOf(element) + " is null, i.e. element was not captured by PageFactory initialization");
            return null;
        }
        try {
            text = element.getText();
        } catch (Exception e) {
            System.out.print("exception caught" + e);
        }
        return text;
    }


    public boolean isElementPresentOnScreen(WebElement element) {
        boolean isElementPresent = false;
        try {
            element.isDisplayed();
            isElementPresent = true;
        } catch (Exception e) {
            isElementPresent = false;
        }
        return isElementPresent;
    }


    public boolean waitforWebElement(WebElement element, int timeout) {
        Reporter.log("Waiting for element: " + String.valueOf(element), true);
        boolean found = false;
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.visibilityOf(element));
            found = true;
        } catch (TimeoutException e) {
            System.out.print(String.valueOf(element) + e + "element not found");
        }
        return found;
    }


    public byte[] createScreenshot(String screenshotName) {
        try {
            if (screenshotName != null && !screenshotName.endsWith(".jpg") && !screenshotName.endsWith(".png") && !screenshotName.endsWith(".jpeg")) {
                screenshotName += ".jpeg";
            }
            TakesScreenshot scrShot = ((TakesScreenshot) driver);
            File file = scrShot.getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(file, new File("Screenshot/" + screenshotName));
            Reporter.log(
                    "<a class=\"test-popup-link\" href=\"Screenshot/" + screenshotName + "\">"
                            + "Screenshot here" + "</a>", true);
            FileInputStream fis = new FileInputStream(new File("Screenshot/" + screenshotName));
            byte[] screenshotBytes = IOUtils.toByteArray(fis);
            return screenshotBytes;
        } catch (Exception e) {
            Reporter.log("Not able to store screenshot", true);
        }
        return null;
    }


    public void uploadFile(WebElement element, String path)
    {
        if (element==null)
        {
            Assert.fail(String.valueOf(element) + "is null, i.e. element was not captured by PageFactory initialization");
            return;
        }
        try {
            element.sendKeys(path);
        } catch (Exception e) {
            System.out.print("exception caught" + e);
        }

    }


    public void waitFixedTime(String timeInSeconds) {
        Reporter.log("Adding a deliberate sleep for " + timeInSeconds + " seconds.", true);
        long seconds = Long.parseLong(timeInSeconds);
        try {
            Thread.sleep(1000 * seconds);
        } catch (InterruptedException e) {
            Reporter.log("Interrupt occurred when implementing a sleep for " + timeInSeconds + " seconds.", true);
            Thread.currentThread().interrupt();
        }
    }
}