package utility;


import java.io.*;

public class CombinedEmailReport {

    // We can set the path of input and output html path now from mvn cmd
    //mvn exec:java@combinedemailreport -DoutputHtml="test.html" -DinputHtml="input.html"

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(getValue("inputHtml", "overview-features.html")));
        BufferedWriter writer = new BufferedWriter(new FileWriter(getValue("outputHtml", "Emailreport.html")));

        try {
            String currentLine;
            writer.write("<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "<style>\n" +
                    "table {\n" +
                    "  border-collapse: collapse;\n" +
                    "  width: 100%;\n" +
                    "}\n" +
                    "table, th, td {\n" +
                    "  border: 1px solid black;\n" +
                    "}\n" +
                    "th, td {\n" +
                    "  text-align: left;\n" +
                    "  padding: 8px;\n" +
                    "}\n" +
                    "tr:nth-child(even){background-color: #f2f2f2}\n" +
                    ".failed{\n" +
                    "background-color: #FA8072;\n" +
                    "}\n" +
                    ".passed{\n" +
                    "background-color: #32CD32;\n" +
                    "}\n" +
                    ".total{\n" +
                    "background-color: #C0C0C0;\t\n" +
                    "}\n" +
                    "th{\n" +
                    "background-color: #B0E0E6;\n" +
                    "}\n" +
                    "</style>\n" +
                    "</head>\n" +
                    "<body>");
            writer.write("");
            writer.write("<table id=\"tablesorter\" class=\"stats-table table-hover\">\n" +
                    "\n" +
                    "                <thead>");
            while ((currentLine = reader.readLine()) != null) {
                if (currentLine.contains("<tr class=\"header dont-sort\">")) {
                    writer.write(currentLine);
                    writer.newLine();

                    while ((currentLine = reader.readLine()) != null && !(currentLine.contains("<td>5</td>"))) {
                        writer.write(currentLine);
                        writer.newLine();
                    }
                }
            }
            writer.write("<td></td>\n" + "    </tbody>\n" +
                    "</table>\n" +
                    "</body>\n" +
                    "</html>");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                //Closing the resources
                reader.close();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

   private static String getValue(String envVar, String filename) {
        String workingRepo = System.getProperty("user.dir");
        String defaultPath = workingRepo + File.separator + filename;
        String var = System.getProperty(envVar);
        return (var != null) ? var : defaultPath;
    }

}