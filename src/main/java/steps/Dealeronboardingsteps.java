package steps;

import com.cars24.library.WebLibrary;
import com.cars24.library.WebSessionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;
import screens.DealeronboardingScreen;

/**
 * @author kirtigupta
 */

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.cars24.library.WebSessionManager.dataProp;
import static com.cars24.library.WebSessionManager.dealerProp;

public class Dealeronboardingsteps {
    private WebDriver driver = WebSessionManager.getInstance().driver;
    private DealeronboardingScreen dealeronboardingScreen = new DealeronboardingScreen(driver);
    private WebLibrary webLibrary = new WebLibrary(driver);
    JavascriptExecutor js = (JavascriptExecutor) driver;

    @Given("^User navigates to Customer Database$")
    public void navigatetoCustomerDatabase() {
        Reporter.log("navigates to customer database", true);
        webLibrary.waitforWebElement(dealeronboardingScreen.getCustomerDatabase(), Integer.parseInt(dataProp.getProperty("DEFAULT_OBJECT_WAIT_TIME")));
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(dealeronboardingScreen.getCustomerDatabase()), "not found customer database on navigation bar");
        webLibrary.clickonWebElement(dealeronboardingScreen.getCustomerDatabase());
    }

    @When("^User navigates to Dealers grid$")
    public void navigatetoDealerGrid() {
        Reporter.log("navigates to Dealer grid", true);
        webLibrary.clickonWebElement(dealeronboardingScreen.getDealer());

    }

    @And("^User clicks on Add Unnati Lite button$")
    public void clickOnaddUnnatibutton() throws InterruptedException {
        webLibrary.clickonWebElement(dealeronboardingScreen.getAddUnnatiLite());
        webLibrary.clickonWebElement(dealeronboardingScreen.getDealerDetails());
        webLibrary.clickonWebElement(dealeronboardingScreen.getDealerId());
        enterDealerCode(dealerProp.getProperty("dfdealercode"));
        Reporter.log("fetching details for the dealercode", true);
        webLibrary.clickonWebElement(dealeronboardingScreen.getFetchFromOms());
        Thread.sleep(2000);

        Select region = new Select(dealeronboardingScreen.getRegionName());
        region.selectByIndex(region.getOptions().size() - 1);

        Thread.sleep(2000);
        js.executeScript("arguments[0].scrollIntoView(true);", dealeronboardingScreen.getGender());

        Select gender = new Select(dealeronboardingScreen.getGender());
        gender.selectByIndex(gender.getOptions().size() - 1);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        webLibrary.enterText(dealeronboardingScreen.getDob(), "22/02/2020");

        Select entityCons = new Select(dealeronboardingScreen.getEntityConstitution());
        entityCons.selectByIndex(entityCons.getOptions().size() - 1);
        Thread.sleep(2000);

        Select businessCategory = new Select(dealeronboardingScreen.getBusinessOperationtype());
        businessCategory.selectByIndex(businessCategory.getOptions().size() - 2);
        Thread.sleep(2000);


    }


    @And("^User clicks on save button of Customer Details Accordion$")
    public void saveCustomerDetails() {
        webLibrary.waitforWebElement(dealeronboardingScreen.getSave(), Integer.parseInt(dataProp.getProperty("DEFAULT_OBJECT_WAIT_TIME")));
        Reporter.log("saving customer detail form", true);
        webLibrary.clickonWebElement(dealeronboardingScreen.getSave());
    }

    @And("^User verifies status becomes inProcess$")
    public void changeStatus() {
        Reporter.log("status changed", true);
        Assert.assertTrue(successfulChangeStatus(), "status changes to inprocess");
    }

    @And("^User creates an offer for the dealer$")
    public void offerCreation()
    {

    }

    public boolean successfulChangeStatus() {
        if (webLibrary.isElementPresentOnScreen(dealeronboardingScreen.getEnlistingForm())) {
            Reporter.log("status changes successfully on submitting customer details", true);
            return true;
        } else {
            Reporter.log("status not changed", true);
            Assert.fail("Status does not changed");
            return false;
        }

    }

    @When("^User navigates to Enlisting form accordion$")
    public void enlistingForm() {
        webLibrary.clickonWebElement(dealeronboardingScreen.getEnlistingForm());
        Reporter.log("clicked on enlisting form accordion", true);
    }

    @When("^User enters text in all fields")
    public void fillEnlistingForm() {
        properietor2Details();
        properietor3Details();
        properietor4Details();
        Select entityConstitution = new Select(dealeronboardingScreen.getEntityConstitution());
        entityConstitution.selectByIndex(entityConstitution.getOptions().size() - 1);
        Reporter.log("dealer type selected", true);
        webLibrary.enterText(dealeronboardingScreen.getEmail(), dealerProp.getProperty("dfdealermailid"));
        webLibrary.enterText(dealeronboardingScreen.getPhoneNo(), dealerProp.getProperty("dfdealermobile"));
        Select cutter = new Select(dealeronboardingScreen.getCutter());
        cutter.selectByIndex(cutter.getOptions().size() - 1);
        Reporter.log("business category of the dealer selected", true);
    }


    @Then("^User clicks on save button and validates status changed to inProcess$")
    public void clickEnlistingForm() {
        webLibrary.clickonWebElement(dealeronboardingScreen.getEnlistingSubmit());
        Reporter.log("dealer's enlisting form saved successfully", true);
    }

    @Given("^User navigates to FI document accordion form, upload documents$")
    public void fiDocumentDetails() throws AWTException, InterruptedException {

        webLibrary.clickonWebElement(dealeronboardingScreen.getFiDoc());
        js.executeScript("arguments[0].scrollIntoView(true);", dealeronboardingScreen.getPanCardDocument());
        js.executeScript("document.getElementById('1_comments').value='pancard';");
        js.executeScript("arguments[0].click();", dealeronboardingScreen.getPanCardDocument());
        webLibrary.uploadFile(dealeronboardingScreen.getPanCardDocument(), System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "images" + File.separator + "abc.png");
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_ESCAPE);
        robot.keyRelease(KeyEvent.VK_ESCAPE);
        webLibrary.waitFixedTime("10");
        webLibrary.clickonWebElement(dealeronboardingScreen.getUploadPanCard());
        Reporter.log("uploaded pan card image", true);
        js.executeScript("arguments[0].scrollIntoView(true);", dealeronboardingScreen.getAadharCardComments());
        js.executeScript("document.getElementById('2_comments').value='aadharcard';");
        js.executeScript("arguments[0].click();", dealeronboardingScreen.getAadharCardDocument());
        webLibrary.uploadFile(dealeronboardingScreen.getAadharCardDocument(), System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "images" + File.separator + "aadhar.png");
        robot.keyPress(KeyEvent.VK_ESCAPE);
        robot.keyRelease(KeyEvent.VK_ESCAPE);
        webLibrary.waitFixedTime("10");
        webLibrary.clickonWebElement(dealeronboardingScreen.getUploadAadharCard());
        Reporter.log("uploaded aadhar card image", true);
        js.executeScript("arguments[0].scrollIntoView(true);", dealeronboardingScreen.getDealerPicComments());
        js.executeScript("document.getElementById('3_comments').value='dealerpic'");
        js.executeScript("arguments[0].click();", dealeronboardingScreen.getDealerPic());
        webLibrary.uploadFile(dealeronboardingScreen.getDealerPic(), System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "images" + File.separator + "abc.png");
        robot.keyPress(KeyEvent.VK_ESCAPE);
        robot.keyRelease(KeyEvent.VK_ESCAPE);
        webLibrary.waitFixedTime("10");
        webLibrary.clickonWebElement(dealeronboardingScreen.getDealerPicUpload());
        Reporter.log("dealerPic Uploaded", true);


        js.executeScript("arguments[0].scrollIntoView(true);", dealeronboardingScreen.getCoapplicantComments());
        js.executeScript("document.getElementById('4_comments').value='dealerpic'");
        js.executeScript("arguments[0].click();", dealeronboardingScreen.getCoapplicantDoc());
        webLibrary.uploadFile(dealeronboardingScreen.getCoapplicantDoc(), System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "images" + File.separator + "abc.png");
        robot.keyPress(KeyEvent.VK_ESCAPE);
        robot.keyRelease(KeyEvent.VK_ESCAPE);
        webLibrary.waitFixedTime("10");
        webLibrary.clickonWebElement(dealeronboardingScreen.getCoapplicantUpload());

        Reporter.log("coapplicant doc uploaded", true);


    }

    @Given("^User selects packages, status becomes Credit Agreement$")
    public void packageSelection() {
        webLibrary.waitFixedTime("3");
        js.executeScript("arguments[0].scrollIntoView(true);", dealeronboardingScreen.getPackageAccordion());
        webLibrary.clickonWebElement(dealeronboardingScreen.getPackageAccordion());
        Select packageSelect = new Select(dealeronboardingScreen.getPackageSelection());
        packageSelect.selectByIndex(packageSelect.getOptions().size() - 1);
        webLibrary.waitFixedTime("2");
        Reporter.log("package selected successfully", true);
        js.executeScript("arguments[0].scrollIntoView(true);", dealeronboardingScreen.getPackageSubmit());
        webLibrary.clickonWebElement(dealeronboardingScreen.getPackageSubmit());
        Reporter.log("package submitted successfully", true);

    }

    public void disableNach() {
        Reporter.log("disable Nach", true);
        if (webLibrary.isElementPresentOnScreen(dealeronboardingScreen.getPackageSelectionMessage())) {
            webLibrary.waitFixedTime("2");
        }
        Reporter.log("naviagtes to CD", true);
        dealeronboardingScreen.getCustomerDatabase().click();
        Reporter.log("clicked on CD", true);

        webLibrary.waitFixedTime("2");
        Reporter.log("navigates to Dealer Config", true);
        dealeronboardingScreen.getDealerConfig().click();
        Reporter.log("clicked on Dealer Config", true);

        webLibrary.waitFixedTime("2");
        Reporter.log("navigates to Dealer code column", true);
        dealeronboardingScreen.getDealerCodeColumn().click();
        Reporter.log("clicked on dealer column in offer Grid", true);

        webLibrary.waitFixedTime("2");
        webLibrary.enterText(dealeronboardingScreen.getDealerCodeColumn(), dealerProp.getProperty("dfdealercode"));
        Reporter.log("dealercode entered", true);

        webLibrary.waitFixedTime("2");
        Reporter.log("userclicks on dealerconfigview", true);
        dealeronboardingScreen.getDealerConfigViewClick().click();
        webLibrary.waitFixedTime("2");
        Reporter.log("clicked on dealer config view", true);


        webLibrary.waitFixedTime("2");
        ArrayList<String> list = new ArrayList<>(driver.getWindowHandles());
        driver.close();
        driver.switchTo().window(list.get(1));
        js.executeScript("arguments[0].click();", dealeronboardingScreen.getNachAccordion());
        Reporter.log("User clicks on Nach Accordion", true);


        Select nachConfig = new Select(dealeronboardingScreen.getIsNachMandatory());
        nachConfig.selectByIndex(nachConfig.getOptions().size() - 1);
        Reporter.log("user selects nach as Disable", true);
        js.executeScript("arguments[0].click();", dealeronboardingScreen.getNachUpdate());

        Reporter.log("user clicks on nachUpdate", true);

        //  webLibrary.waitFixedTime("5");


        if (webLibrary.isElementPresentOnScreen(dealeronboardingScreen.getDealerConfigurationPage())) ;
        {
            webLibrary.waitFixedTime("4");
            driver.get("http://df-qa26.ninja24.in/applicants/applicant-config/index");
            driver.navigate().refresh();
            webLibrary.waitFixedTime("5");
            Reporter.log("dealer config grid refreshing", true);

        }

    }

    @Given("^User navigates to Enlisting Document Uplaod accordion, status becomes Enlistment Pending$")
    public void enlistingDocumentUpload() {

        disableNach();
        webLibrary.waitFixedTime("2");

        js.executeScript("arguments[0].click();", dealeronboardingScreen.getCustomerDatabase());
        js.executeScript("arguments[0].click();", dealeronboardingScreen.getDealer());
        Reporter.log("clicked on Dealers", true);
        js.executeScript("arguments[0].click();", dealeronboardingScreen.getDealerCodeColumnDealerGrid());
        webLibrary.enterText(dealeronboardingScreen.getDealerCodeColumnDealerGrid(), dealerProp.getProperty("dfdealercode"));
        Reporter.log("clicked on Dealers Grid column", true);
        js.executeScript("arguments[0].click();", dealeronboardingScreen.getDealerCodeLink());
        js.executeScript("arguments[0].click();", dealeronboardingScreen.getEnlistingDocumentUpload());

        Reporter.log("navigated to enlisting Document", true);
        js.executeScript("arguments[0].scrollIntoView(true);", dealeronboardingScreen.getEnlistingDocumentUpload());


        webLibrary.uploadFile(dealeronboardingScreen.getSignedEnlistingForm(), System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "images" + File.separator + "abc.png");
        String cssSelectorOfSameElements = "input[type='submit'][id='button']";
        List<WebElement> a = driver.findElements(By.cssSelector(cssSelectorOfSameElements));
        List<WebElement> mediums = new ArrayList<>();
        mediums.set(0, a.get(0)).click();

        System.out.print(a);
        //  a.get(0).click();
        webLibrary.uploadFile(dealeronboardingScreen.getCreditAgreement(), System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "images" + File.separator + "abc.png");
        // a.get(1).click();
        mediums.set(1, a.get(1)).click();
        webLibrary.uploadFile(dealeronboardingScreen.getBlankCheque(), System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "images" + File.separator + "abc.png");
        // a.get(2).click();
        mediums.set(2, a.get(2)).click();
        dealeronboardingScreen.getEnlistingDocumentSubmit().click();

    }

    public void enterDealerCode(String dealercode) {
        webLibrary.enterText(dealeronboardingScreen.getDealerId(), dealercode);
    }

    public void properietor2Details() {
        js.executeScript("arguments[0].scrollIntoView(true);", dealeronboardingScreen.getProperietor2());
        Reporter.log("properiter2 found", true);
        webLibrary.enterText(dealeronboardingScreen.getProperietor2(), dealerProp.getProperty("dfProperietor2"));
        Select gender2 = new Select(dealeronboardingScreen.getOwner2Gender());
        gender2.selectByIndex(gender2.getOptions().size() - 1);
        webLibrary.enterText(dealeronboardingScreen.getPanProp2(), dealerProp.getProperty("dfPanProp2"));
        webLibrary.enterText(dealeronboardingScreen.getAddharProp2(), dealerProp.getProperty("dfAadharcard2"));
        Reporter.log("owner2 aadhar card no", true);

    }

    public void properietor3Details() {
        Reporter.log("scrolling to owner3 field", true);
        js.executeScript("arguments[0].scrollIntoView(true);", dealeronboardingScreen.getProp3());
        webLibrary.enterText(dealeronboardingScreen.getProp3(), dealerProp.getProperty("dfProperietor3"));
        Select gender3 = new Select(dealeronboardingScreen.getOwner3Gender());
        gender3.selectByIndex(gender3.getOptions().size() - 1);
        webLibrary.enterText(dealeronboardingScreen.getPanProp3(), dealerProp.getProperty("dfPanProp3"));
        webLibrary.enterText(dealeronboardingScreen.getAadharProp3(), dealerProp.getProperty("dfAadharcard3"));
        Reporter.log("owner aadhar card no", true);
    }

    public void properietor4Details() {
        webLibrary.enterText(dealeronboardingScreen.getProp4(), dealerProp.getProperty("dfProperietor4"));
        Select gender4 = new Select(dealeronboardingScreen.getOwner4Gender());
        gender4.selectByIndex(gender4.getOptions().size() - 1);
        webLibrary.enterText(dealeronboardingScreen.getPanProp4(), dealerProp.getProperty("dfPanProp4"));
        webLibrary.enterText(dealeronboardingScreen.getAadharProp4(), dealerProp.getProperty("dfAadharcard4"));
        Reporter.log("owner4 aadhar card no", true);
    }


}
