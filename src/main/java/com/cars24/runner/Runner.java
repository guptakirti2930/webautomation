package com.cars24.runner;

import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import java.io.File;

/**
 * @author kirtigupta
 */

@RunWith(Cucumber.class)
@CucumberOptions(glue = "steps", features = "src/main/resources/features",
        plugin = {"com.vimalselvam.cucumber.listener.ExtentCucumberFormatter:output/report.html"})

public class Runner {
    WebDriver driver;

    @BeforeClass
    public static void setup() {

    }

    @AfterClass
    public static void teardown() {
        String path = System.getProperty("user.dir") + File.separator + "extent-config.xml";
        File file = new File(path);
        Reporter.loadXMLConfig(file);
        Reporter.setSystemInfo("user", System.getProperty("user.name"));
        Reporter.setSystemInfo("os", "Windows");
        Reporter.setTestRunnerOutput("Sample test com.cars24.library.runner output message");
        Reporter.assignAuthor(System.getProperty("user.name"));
        Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));

    }

}
