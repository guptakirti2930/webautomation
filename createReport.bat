cd "\Program Files (x86)\Jenkins\jobs\C2CSmokeTest\builds\%BUILD_NUMBER%\cucumber-html-reports\"
set "pwd=%cd%"
del "%WORKSPACE%\overview-features.html"
copy "%pwd%\overview-features.html" "%WORKSPACE%\overview-features.html"
cd "%WORKSPACE%"
mvn clean install
mvn exec:java@combinedemailreport