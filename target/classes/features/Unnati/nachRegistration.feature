#author : Kirti Gupta
#Prerequisites : should be log into df panel and Dealer has mapping with Bank.

@NachRegistration
Feature: Nach Registration
  As a user,
  I want to do nach registration for a dealer in DF panel

  Background:
    Given User is logged in on Unnati Panel


  @papernach
  Scenario: User register a dealer for paperNach registration
    Given User navigates to eNach Management, clicks on Registration grid
    And User clicks on paperNach button
    When User fills all the details in all accordions
    Then User verifies enach registered for dealer

