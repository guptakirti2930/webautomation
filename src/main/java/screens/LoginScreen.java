package screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author kirtigupta
 */


//pageelements and methods
public class LoginScreen {

    public LoginScreen(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "userName")
    private WebElement username;

    @FindBy(id = "password")
    private WebElement password;

    @FindBy(id = "submitbutton")
    private WebElement submit;

    @FindBy(xpath = "//div[@class=\"col-md-12\"]/div/div/h3[@class=\"card-title\"]")
    private WebElement successfullyLoggedIn;

    public WebElement getUsername() {
        return username;
    }

    public WebElement getPassword() {
        return password;
    }

    public WebElement getSubmitButton() {
        return submit;
    }

    public WebElement getSuccessfullyLoggedIn() {
        return successfullyLoggedIn;
    }
}
