package steps;

import com.cars24.library.WebLibrary;
import com.cars24.library.WebSessionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import screens.NachRegistrationScreen;
import static com.cars24.library.WebSessionManager.dealerProp;

import java.io.File;

public class Nachregistrationsteps {
    private WebDriver driver = WebSessionManager.getInstance().driver;
    private NachRegistrationScreen nachRegistrationScreen = new NachRegistrationScreen(driver);
    private WebLibrary webLibrary = new WebLibrary(driver);
    JavascriptExecutor js = (JavascriptExecutor) driver;

    @Given("^User navigates to eNach Management, clicks on Registration grid$")
    public void naviagteToNachManagement() {
        Reporter.log("click on enach management menu", true);


        webLibrary.waitFixedTime("2");
        Reporter.log("waiting for enach management menu", true);
        if (webLibrary.isElementPresentOnScreen(nachRegistrationScreen.getEnachMenu())) {
            Reporter.log("clicking on enach menu", true);
            webLibrary.clickonWebElement(nachRegistrationScreen.getEnachMenu());
            webLibrary.clickonWebElement(nachRegistrationScreen.getRegistrationMenu());
            Reporter.log("navigates to registration menu", true);
        }

    }

    @And("^User clicks on paperNach button$")
    public void clickPaperNach() {
        webLibrary.waitFixedTime("2");
        webLibrary.clickonWebElement(nachRegistrationScreen.getPaperNachButton());

    }

    @When("^User fills all the details in all accordions$")
    public void detailsPaperNachAccordion() {
        paperNachdetailsAccordionDetails();
        verificationPaperNachAccordion();
        dispatchDetailsAccordion();
        documentReceivedDetails();
        sendToBank();
        bankApproval();

    }

    @Then("^User verifies enach registered for dealer$")
    public void statusCheck() {
        Reporter.log("status becomes register success", true);
    }


    public void paperNachdetailsAccordionDetails() {
        webLibrary.clickonWebElement(nachRegistrationScreen.getPaperNachDetailsAccordion());
        Select dealerBank = new Select(nachRegistrationScreen.getSelectDealerBank());
        dealerBank.selectByIndex(1);
        webLibrary.uploadFile(nachRegistrationScreen.getUploadDocument(), System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "images" + File.separator + "abc.png");

        js.executeScript("arguments[0].scrollIntoView(true);", nachRegistrationScreen.getPaperNachDetailsSubmit());
        webLibrary.clickonWebElement(nachRegistrationScreen.getPaperNachDetailsSubmit());
        Reporter.log("first accordion saved", true);

    }

    public void verificationPaperNachAccordion() {
        webLibrary.waitFixedTime("2");
        webLibrary.clickonWebElement(nachRegistrationScreen.getVerificationPaperNach());
        if (webLibrary.isElementPresentOnScreen(nachRegistrationScreen.getPaperNachVerifiationDoc())) {
            js.executeScript("arguments[0].scrollIntoView(true);", nachRegistrationScreen.getVerificationPaperNachSubmit());

            js.executeScript("arguments[0].click();", nachRegistrationScreen.getVerificationPaperNachSubmit());
            Reporter.log("verification button visible", true);
            webLibrary.waitFixedTime("2");
            webLibrary.waitFixedTime("3");
            Reporter.log("nach registration pop up submit", true);
            webLibrary.waitFixedTime("3");
            webLibrary.clickonWebElement(nachRegistrationScreen.getPopupSubmit());
            Reporter.log("pop up box submit", true);
        }

    }

    public void dispatchDetailsAccordion() {
        webLibrary.waitFixedTime("3");
        webLibrary.clickonWebElement(nachRegistrationScreen.getDispatchDetails());
        js.executeScript("arguments[0].click();", nachRegistrationScreen.getDispatchDate());
        webLibrary.enterText(nachRegistrationScreen.getDispatchDate(),dealerProp.getProperty("dispatchdate"));
        js.executeScript("arguments[0].scrollIntoView(true);", nachRegistrationScreen.getCourierName());
        js.executeScript("arguments[0].click();", nachRegistrationScreen.getCourierName());
        webLibrary.enterText(nachRegistrationScreen.getCourierName(),dealerProp.getProperty("couriername"));
        js.executeScript("arguments[0].click();", nachRegistrationScreen.getTrackingId());
        webLibrary.enterText(nachRegistrationScreen.getTrackingId(), dealerProp.getProperty("trackingid"));
        js.executeScript("arguments[0].click();", nachRegistrationScreen.getDispatchSubmit());
        if (webLibrary.isElementPresentOnScreen(nachRegistrationScreen.getDispatchdetailPopupSubmit())) {
            webLibrary.clickonWebElement(nachRegistrationScreen.getDispatchdetailPopupSubmit());
        }

    }

    public void documentReceivedDetails() {
        webLibrary.waitFixedTime("3");
        webLibrary.clickonWebElement(nachRegistrationScreen.getDocumentReceivedDetails());
        js.executeScript("arguments[0].scrollIntoView(true);", nachRegistrationScreen.getDocumentReceivedDate());
        js.executeScript("arguments[0].click();", nachRegistrationScreen.getDocumentReceivedDate());
        webLibrary.enterText(nachRegistrationScreen.getDocumentReceivedDate(), dealerProp.getProperty("documentreceiveddate"));
        js.executeScript("arguments[0].click();", nachRegistrationScreen.getDocumentReceivedSubmit());
        if (webLibrary.isElementPresentOnScreen(nachRegistrationScreen.getDocumentPopup())) {
            webLibrary.clickonWebElement(nachRegistrationScreen.getDocumentPopupSubmit());
        }

    }

    public void sendToBank() {
        webLibrary.waitFixedTime("2");
        js.executeScript("arguments[0].scrollIntoView(true);", nachRegistrationScreen.getBankDetails());
        webLibrary.clickonWebElement(nachRegistrationScreen.getBankDetails());
        js.executeScript("arguments[0].scrollIntoView(true);", nachRegistrationScreen.getMaxAmount());
        webLibrary.enterText(nachRegistrationScreen.getMaxAmount(), dealerProp.getProperty("maxamount"));
        webLibrary.uploadFile(nachRegistrationScreen.getDocumentSendToBank(), System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "images" + File.separator + "abc.png");
        webLibrary.waitFixedTime("2");
        js.executeScript("arguments[0].click();", nachRegistrationScreen.getSendToBankSubmitButton());
        js.executeScript("arguments[0].click();", nachRegistrationScreen.getSendToBankPopupSubmit());


    }

    public void bankApproval() {
        webLibrary.waitFixedTime("2");
        js.executeScript("arguments[0].scrollIntoView(true);", nachRegistrationScreen.getBankApproval());
        js.executeScript("arguments[0].click();", nachRegistrationScreen.getBankApproval());
        webLibrary.clickonWebElement(nachRegistrationScreen.getUMRN());
        webLibrary.enterText(nachRegistrationScreen.getUMRN(), dealerProp.getProperty("UMRN"));
        webLibrary.clickonWebElement(nachRegistrationScreen.getBankReceivedDate());
        webLibrary.enterText(nachRegistrationScreen.getBankReceivedDate(),dealerProp.getProperty("bankReceivedDate"));
        js.executeScript("arguments[0].click();", nachRegistrationScreen.getBankApprovalSubmit());
        if (webLibrary.isElementPresentOnScreen(nachRegistrationScreen.getBankApprovalPopup())) {
            js.executeScript("arguments[0].click();", nachRegistrationScreen.getBankApprovalPopupSubmit());
        }
    }


}


