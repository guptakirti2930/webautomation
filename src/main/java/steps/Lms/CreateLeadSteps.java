package steps.Lms;

import com.cars24.library.WebLibrary;
import com.cars24.library.WebSessionManager;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;
import screens.Lms.CreateLeadScreen;

import java.io.File;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ishitachoudhary
 */

// mapping of feature file with gherkin syntax and calling methods of screen and weblibrary class.
public class CreateLeadSteps extends WebSessionManager {

    private WebDriver driver = WebSessionManager.getInstance().driver;
    private CreateLeadScreen lead_screen = new CreateLeadScreen(driver);
    private WebLibrary webLibrary = new WebLibrary(driver);
    private CommonSteps commonsteps = new CommonSteps();
    static String appointment_id;
    static String workingDirectory = System.getProperty("user.dir");
    static String dataDirectoryPath = workingDirectory + File.separator + "data" + File.separator + "LMS-LeadData.properties";
    public static Properties leadProp = propertyFile(dataDirectoryPath);

    @Given("^User is on home menu$")
    public void home_Menu() {
        verifyHomeMenu();
        webLibrary.clickonWebElement(lead_screen.getHomeMenu());
        Reporter.log("User clicks on home menu", true);
    }

    @When("^User clicks on create lead option from menu$")
    public void clickCreateLead() {
        verifyHomeMenuElements();
        webLibrary.clickonWebElement(lead_screen.getLeadOption());
        Reporter.log("User clicks on create Lead Option", true);
        Reporter.log("User successfully navigates to create lead", true);
    }

    @Given("^User is on create lead page$")
    public void lead_Page() {
        home_Menu();
        clickCreateLead();
        Reporter.log("User is on create lead page", true);
    }

    @When("^User clicks on create new lead button without entering any data$")
    public void verifyValidation() {
        verifyCreateLeadOption();
        webLibrary.clickonWebElement(lead_screen.getLeadButton());
        Reporter.log("User clicks on create lead button", true);
    }

    @Then("^User should get validation error messages on every field$")
    public void errorMessages() {
        verifyErrorMessageElements();
        Reporter.log("User gets error messages", true);
    }

    @When("^User fills book appointment form$")
    public void bookAppt(DataTable lead_data) {
        verifyLeadPageElements();
        enterCustDetails(lead_data);
        enterCarDetails();
        enterStoreDetails(lead_data);
    }

    @And("^User clicks on create new lead button$")
    public void clickLeadButton() {
        webLibrary.clickonWebElement(lead_screen.getLeadButton());
        Reporter.log("User clicks on create lead button", true);
    }

    @Then("^User should successfully be able to create an appointment$")
    public void bookAppt() {
        verifyPopupElements();
        String appt_id = webLibrary.fetchText(lead_screen.getApptId());
        Reporter.log(appt_id, true);
        Pattern pattern = Pattern.compile("ID ([^\\s]+)");
        Matcher matcher = pattern.matcher(appt_id);
        String appointment = null;
        while (matcher.find()) {
            appointment = matcher.group(1);
        }
        Reporter.log(">>>>>>>>>>>" + appointment, true);
        appointment_id = appointment;
        webLibrary.clickonWebElement(lead_screen.getOkayBtn());
    }

    public void verifyHomeMenu() {
        webLibrary.waitforWebElement(lead_screen.getHomeMenu(), Integer.parseInt(dataProp.getProperty("DEFAULT_OBJECT_WAIT_TIME")));
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(lead_screen.getHomeMenu()), "Verify home button is present");
    }

    public void verifyHomeMenuElements() {
        webLibrary.waitforWebElement(lead_screen.getLeadOption(), Integer.parseInt(dataProp.getProperty("DEFAULT_OBJECT_WAIT_TIME")));
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(lead_screen.getLeadOption()), "Verify create Lead option present");
    }

    public void verifyCreateLeadOption() {
        webLibrary.waitforWebElement(lead_screen.getLeadButton(), Integer.parseInt(dataProp.getProperty("DEFAULT_OBJECT_WAIT_TIME")));
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(lead_screen.getLeadButton()), "Verify create new lead button present");
    }

    public void verifyErrorMessageElements() {
        webLibrary.waitforWebElement(lead_screen.getNameError(), Integer.parseInt(dataProp.getProperty("DEFAULT_OBJECT_WAIT_TIME")));
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(lead_screen.getNameError()), "Verify name error message is present");
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(lead_screen.getPhoneError()), "Verify phone error message is present");
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(lead_screen.getVariantError()), "Verify variant error message is present");
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(lead_screen.getAwarenessError()), "Verify awareness error message is present");
    }

    public void verifyLeadPageElements() {
        webLibrary.waitforWebElement(lead_screen.getCustomerName(), Integer.parseInt(dataProp.getProperty("DEFAULT_OBJECT_WAIT_TIME")));
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(lead_screen.getCustomerName()), "Verify customer name is present");
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(lead_screen.getCarModel()), "Verify car model is present");
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(lead_screen.getCarState()), "Verify car state is present");
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(lead_screen.getApptDate()), "Verify appointment date is present");
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(lead_screen.getCarReason()), "Verify selling reason is presenr");
    }

    public void enterCustDetails(DataTable lead_data) {
        List<String> data = lead_data.asList(String.class);
        lead_screen.getCustomerName().sendKeys(data.get(0));
        lead_screen.getCustomerPhone().sendKeys(data.get(1));
        lead_screen.getCustomerEmail().sendKeys(data.get(2));
        webLibrary.waitforWebElement(lead_screen.getNameError(), Integer.parseInt(dataProp.getProperty("SHORT_WAIT_TIME")));
    }

    public void enterCarDetails() {

        Select makeSelect = new Select(lead_screen.getCarMake());
        makeSelect.selectByVisibleText(leadProp.getProperty("make"));
        webLibrary.waitforWebElement(lead_screen.getNameError(), Integer.parseInt(dataProp.getProperty("SHORT_WAIT_TIME")));
        Select modelSelect = new Select(lead_screen.getCarModel());
        modelSelect.selectByVisibleText(leadProp.getProperty("model"));
        webLibrary.waitforWebElement(lead_screen.getNameError(), Integer.parseInt(dataProp.getProperty("SHORT_WAIT_TIME")));
        Select yearSelect = new Select(lead_screen.getCarYear());
        yearSelect.selectByVisibleText(leadProp.getProperty("year"));
        Select variantSelect = new Select(lead_screen.getCarVariant());
        variantSelect.selectByVisibleText(leadProp.getProperty("variant"));
        Select odometerSelect = new Select(lead_screen.getCarOdometer());
        odometerSelect.selectByVisibleText(leadProp.getProperty("odometer"));
        Select stateSelect = new Select(lead_screen.getCarState());
        stateSelect.selectByVisibleText(leadProp.getProperty("registration_state"));
    }

    public void enterStoreDetails(DataTable lead_data) {
        List<String> data = lead_data.asList(String.class);
        Select citySelect = new Select(lead_screen.getStoreCity());
        citySelect.selectByVisibleText(leadProp.getProperty("store_city"));
        webLibrary.waitforWebElement(lead_screen.getNameError(), Integer.parseInt(dataProp.getProperty("SHORT_WAIT_TIME")));
        Select storeSelect = new Select(lead_screen.getStoreName());
        storeSelect.selectByVisibleText(leadProp.getProperty("store"));
        webLibrary.waitforWebElement(lead_screen.getNameError(), Integer.parseInt(dataProp.getProperty("SHORT_WAIT_TIME")));
        Select dateSelect = new Select(lead_screen.getApptDate());
        dateSelect.selectByIndex(1);
        webLibrary.waitforWebElement(lead_screen.getNameError(), Integer.parseInt(dataProp.getProperty("SHORT_WAIT_TIME")));
        Select timeSelect = new Select(lead_screen.getApptTime());
        timeSelect.selectByIndex(1);
        Select sourceSelect = new Select(lead_screen.getSourceAwareness());
        sourceSelect.selectByVisibleText(leadProp.getProperty("source"));
        lead_screen.getCarReason().sendKeys(data.get(3));
    }

    public void verifyPopupElements() {
        webLibrary.waitforWebElement(lead_screen.getApptPopup(), Integer.parseInt(dataProp.getProperty("DEFAULT_OBJECT_WAIT_TIME")));
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(lead_screen.getApptPopup()), "Verify appointment popup is present");
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(lead_screen.getOkayBtn()), "Verify ok button is present");
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(lead_screen.getCopyApptBtn()), "Verify copy button is present");
    }
}
