package steps.Lms;

import com.cars24.library.WebLibrary;
import com.cars24.library.WebSessionManager;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import steps.Lms.LoginSteps;

import java.io.File;
import java.io.IOException;

/**
 * @author ishitachoudhary
 */

public class CommonSteps {

    private WebDriver driver = WebSessionManager.getInstance().driver;
    private WebLibrary webLibrary = new WebLibrary(driver);
    private LoginSteps loginsteps = new LoginSteps();
    private File login = new File("loginFolder/" + "login.txt");
    private File loginFolder = new File("loginFolder/");

    @Given("^User is already logged in on LMS$")
    public void userLogIn() {
        deleteLoginFolder();
        if (!login.exists()) {
            loginsteps.user_login_page();
            loginsteps.enter_credentials("valid", "internal user");
            loginsteps.click_login_btn();
            loginsteps.verifyLogin("successful");
            Reporter.log("User logs into LMS!!", true);
        } else
            Reporter.log("User is already logged in\n", true);
    }

    public void deleteLoginFolder() {
        Reporter.log("Deleting Login folder!", true);
        try {
            FileUtils.deleteDirectory(loginFolder);
        } catch (IOException e) {
            e.printStackTrace();
            Reporter.log("File does not exist", true);
        }
    }
}
