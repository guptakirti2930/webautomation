package com.cars24.library;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class C2CWebLibrary {

    private WebDriver driver;
    ElementLocator elementlocator = new ElementLocator();

    public C2CWebLibrary(WebDriver driver) {
        this.driver = driver;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);
    }

    public void click(String element) {
        try {
            new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(elementlocator.elements(element))).click();
            Reporter.log("*****Performed click operation on element->" + elementlocator + "*****", true);
        } catch (ElementNotVisibleException env) {
            driver.findElement(elementlocator.elements(element)).click();
            Reporter.log(env.getMessage(), true);
            Reporter.log("*****Element is present in DOM but not visible on the page*****" + env.getMessage(), true);
        } catch (NoSuchElementException ne) {
            Reporter.log(ne.getMessage(), true);
            Reporter.log("*****The element could not be located on the page.*****" + ne.getMessage(), true);
        } catch (StaleElementReferenceException se) {
            Reporter.log(se.getMessage(), true);
            Reporter.log(
                    "*****Either the element has been deleted entirely or the element is no longer attached to DOM.*****"
                            + se.getMessage(), true);
        } catch (Exception e) {
            Reporter.log(e.getMessage(), true);
            Reporter.log("*****Could not perform click. Please check!!! *****" + e.getMessage(), true);
        }
    }

    public void fill(String element, String inputdata) {
        try {
            new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(elementlocator.elements(element))).sendKeys(inputdata);
            Reporter.log("*****Performed fill operation on locator->" + element + "*****", true);
        } catch (InvalidElementStateException ie) {
            Reporter.log(ie.getMessage(), true);
            Reporter.log("*****Element is either hidden or disabled*****", true);
        } catch (NoSuchElementException ne) {
            Reporter.log(ne.getMessage(), true);
            Reporter.log("*****The element could not be located on the page.*****" + ne.getMessage(), true);
        } catch (StaleElementReferenceException se) {
            Reporter.log(
                    "*****Either the element has been deleted entirely or the element is no longer attached to DOM.*****"
                            + se.getMessage(), true);
        } catch (Exception e) {
            Reporter.log(e.getMessage(), true);
            Reporter.log("*****Could not perform fill operation. Please check!!! *****" + e.getMessage(), true);
        }
    }

    public void clear(String element) {
        try {
            driver.findElement(elementlocator.elements(element)).clear();
            Reporter.log("*****Performed fill operation on locator->" + element + "*****", true);
        } catch (InvalidElementStateException ie) {
            Reporter.log(ie.getMessage(), true);
            Reporter.log("*****Element is either hidden or disabled*****", true);
        } catch (NoSuchElementException ne) {
            Reporter.log(ne.getMessage(), true);
            Reporter.log("*****The element could not be located on the page.*****" + ne.getMessage(), true);
        } catch (StaleElementReferenceException se) {
            Reporter.log(
                    "*****Either the element has been deleted entirely or the element is no longer attached to DOM.*****"
                            + se.getMessage(), true);
        } catch (Exception e) {
            Reporter.log(e.getMessage(), true);
            Reporter.log("*****Could not perform fill operation. Please check!!! *****" + e.getMessage(), true);
        }
    }

    public String getText(String locator) {
        WebDriverWait wait = new WebDriverWait(driver, 90);
        WebElement element = wait
                .until(ExpectedConditions.visibilityOfElementLocated(elementlocator.elements(locator)));
        return element.getText();
    }

    public void scrollElement(WebDriver driver) {
        try {
            JavascriptExecutor jsExecutor = ((JavascriptExecutor) driver);
            long lastHeight = (long) jsExecutor.executeScript("return document.body.scrollHeight");
            int y = 300;
            int x = 150;
            long initialOffset = 0;
            long finalOffset = 0;
            while (y < lastHeight) {
                jsExecutor.executeScript("window.scrollTo(" + x + "," + y + ");", "");
                y = y + 100;
                Thread.sleep(300);
                finalOffset = getYOffset(driver, y);
                if (!isPageScrolling(initialOffset, finalOffset, lastHeight)) {
                    //TODO create a method to close any popup which is blocking scroll functionality
                }
                initialOffset = finalOffset;
                lastHeight = (long) jsExecutor.executeScript("return document.body.scrollHeight");
            }
        } catch (Exception e) {
            Reporter.log(e.getMessage());
        }

    }

    public boolean isPageScrolling(long initialOffset, long finalOffset, long lastHeight) {
        return ((initialOffset != finalOffset) && finalOffset < lastHeight);
    }

    public long getYOffset(WebDriver driver, int y) {
        try {
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            Long value = (Long) executor.executeScript("return window.pageYOffset;");
            return value;
        } catch (Exception e) {
            e.printStackTrace();
            return 0L;
        }
    }

    public void moveToTop() {
        WebElement element = driver.findElement(By.tagName("header"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);
    }

    public boolean stringContainsItemFromList(String inputStr, List<String> items) {
        for (String str : items) {
            if (inputStr.contains(str)) {
                boolean bool = containsOnce(inputStr, str);
                return bool;
            }
        }
        return false;
    }

    public void enterTextUsingAction(String element, String text) throws InterruptedException {
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(elementlocator.elements(element)));
        actions.click();
        actions.sendKeys(text);
        actions.sendKeys(Keys.TAB);
        actions.build().perform();
    }

    public List<WebElement> getWebelementsList(String element) {
        List<WebElement> elements = null;
        try {
            elements = driver.findElements(elementlocator.elements(element));
        } catch (Exception e) {
            Reporter.log(e.getMessage());
        }
        return elements;

    }

    public void scrollToWebElement(String className) {
        try {
            (new WebDriverWait(driver, 20L)).until(ExpectedConditions.visibilityOfElementLocated(elementlocator.elements(className)));
            WebElement element = driver.findElement(elementlocator.elements(className));
            Point point = element.getLocation();
            JavascriptExecutor jsExecutor = ((JavascriptExecutor) driver);
            long y = point.getY() - 200;
            jsExecutor.executeScript("window.scrollTo(" + point.getX() + "," + y + ");", "");
        } catch (Exception e) {
        }
    }

    public boolean containsOnce(String mainString, String substring) {
        final int i = mainString.indexOf(substring);
        return i != -1 && i == mainString.lastIndexOf(substring);
    }

    public boolean getPriceFromWebElementList(String range, List<WebElement> carList) {
        boolean result = true;
        for (WebElement element : carList) {
            Actions actions = new Actions(driver);
            actions.moveToElement(element).build().perform();
            String cardetail = element.getText();
            if (!verifyPriceRange(range, getPriceFromString(cardetail))) {
                result = false;
                Reporter.log(cardetail, true);
                break;
            }
        }
        return result;
    }

    public int getPriceFromString(String cardetail) {
        Pattern pattern = Pattern.compile("₹([^\\s]+)");
        Matcher matcher = pattern.matcher(cardetail);
        int price = 0;
        while (matcher.find()) {
            price = Integer.parseInt(matcher.group(1).toString().replace(",", ""));
        }
        return price;
    }

    public boolean verifyPriceRange(String range, int price) {

        if (range.equals("Under ₹2 Lakhs") && price > 0 && price <= 200000) {
            return true;
        } else if (range.equals("₹2 - 5 Lakhs") && price > 200000 && price <= 500000) {
            return true;
        } else if (range.equals("₹5 - 10 Lakhs") && price > 500000 && price <= 1000000) {
            return true;
        } else if (range.equals("Above ₹10 Lakhs") && price > 1000000) {
            return true;
        }
        return false;
    }


}
