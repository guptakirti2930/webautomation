#Author - ishita
  #Reviewer -

  #pre-requisite:
  # 1. URL : lms-qa23.ninja24.in
@login-lms
Feature: Login  Functionality Feature
  As a user,
  I want to login into LMS by providing username and password

  @lms-sanity
  Scenario Outline: Login functionality to verify successful and unsuccessful attempts.
    Given User is on login page
    When User enters <valid_invalid> credentials of <type>
    And User clicks on login button
    Then User is <state> to login

    Examples:
      | valid_invalid | type          | state        |
      | valid         | internal user | successful   |
      | invalid       | internal user | unsuccessful |
      | valid         | external user | successful   |
      | invalid       | external user | unsuccessful |