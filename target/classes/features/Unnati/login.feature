#author : Kirti Gupta
#Prerequisites : Should have URL of df panel "http://df-qa26.ninja24.in".
# have login credentials in admin panel.
# Log in credentials synced with LDAP.

# Reviewer: Sanjit singh

@dflogin
Feature: login functionality
  As a user,
  I want to log in into DF panel.

  Scenario Outline: User verifies login functionality
    Given User navigates to login page
    When User enters <username> and <password>
    Then User is <state> logged in


    Examples:
      | username        | password | state        |
      | test@cars24.com | 123456   | successful   |
      | abc@gmail.com   | 123      | unsuccessful |