
package steps;

import com.cars24.library.WebLibrary;
import com.cars24.library.WebSessionManager;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import screens.DealeronboardingScreen;

import java.io.File;
import java.io.IOException;

import static com.cars24.library.WebSessionManager.dataProp;

/**
 * @author kirtigupta
 */

public class Commonsteps {

    private WebDriver driver = WebSessionManager.getInstance().driver;
    private WebLibrary webLibrary = new WebLibrary(driver);
    private Loginsteps loginsteps = new Loginsteps();
    private File login = new File("loginFolder/" + "login.txt");
    private File loginFolder = new File("loginFolder/");

    @Given("^User is logged in on Unnati Panel")
    public void checkAlreadyLoggedIn() {
        deleteLogInFolder();
        if (!loginFolder.exists()) {
            loginsteps.loginpage();
            loginsteps.enterValidCredentials(dataProp.getProperty("dfusername"), dataProp.getProperty("dfpassword"));
            loginsteps.verifyLogin("successful");
            Reporter.log("user successfully loggedin", true);
        } else {
            Reporter.log("user is already loggedin", true);
        }


    }


    public void deleteLogInFolder() {
        Reporter.log("Deleting Login folder", true);
        try {
            FileUtils.deleteDirectory(loginFolder);
        } catch (IOException e) {
            e.printStackTrace();
            Reporter.log("Not able to delete directory", true);
        }
    }


}
