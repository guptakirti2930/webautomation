package steps.C2C;

import com.cars24.library.C2CWebLibrary;
import com.cars24.library.ElementLocator;
import com.cars24.library.WebSessionManager;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.Reporter;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class C2CLandingSteps extends WebSessionManager {

    private WebDriver driver = WebSessionManager.getInstance().driver;
    private C2CWebLibrary webLibrary = new C2CWebLibrary(driver);
    String env = System.getProperty("env");
    String inputEnvUrl = (env != null) ? dataProp.getProperty(env) : dataProp.getProperty("c2c_prod");
    String path = System.getProperty("user.dir") + File.separator + "data" + File.separator
            + "C2C-LandingPage.properties";
    Properties landingPageLocator = propertyFile(path);
    ElementLocator elementLocator = new ElementLocator();

    @Given("^User open c2c website$")
    public void user_open_c_c_website() {
        Reporter.log("input url:" + inputEnvUrl, true);
        driver.get(inputEnvUrl);
    }

    @When("^User click on search$")
    public void user_click_on_search() {
        webLibrary.click(landingPageLocator.getProperty("searchButton"));
    }

    @When("^User enters city \"([^\"]*)\" on landing page$")
    public void user_enter_city(String city) throws InterruptedException {
        webLibrary.enterTextUsingAction(landingPageLocator.getProperty("city"), city);
    }

    @When("^User enters model \"([^\"]*)\" on landing page$")
    public void user_enter_model(String model) throws InterruptedException {
        webLibrary.enterTextUsingAction(landingPageLocator.getProperty("model"), model);
    }

    @When("^User enters budget \"([^\"]*)\" on landing page$")
    public void user_enter_budget(String budget) throws InterruptedException {
        webLibrary.enterTextUsingAction(landingPageLocator.getProperty("budget"), budget);
    }

    @Then("^Listing page should contains car with below filter$")
    public void list_of_car_contains_city_model_and_budget(List<String> list) throws InterruptedException {
        boolean result = false;
        //Scroll page till last so all the cars present on page should load before starting verification
        webLibrary.scrollElement(driver);
        webLibrary.moveToTop();
        //TODO need to fix logix for city = delhi-ncr
        //Get cars list visible on listing page
        List<WebElement> carList = webLibrary.getWebelementsList(landingPageLocator.getProperty("carsList"));
        //Verify car details contains field require
        if (carList != null) {
            for (WebElement webElement : carList) { // now we will verify all the elememts
                Actions actions = new Actions(driver);
                actions.moveToElement(webElement).build().perform();
                String carinfo = webElement.getText();
                result = webLibrary.stringContainsItemFromList(carinfo, list);
                if (!result) {
                    break;
                }
            }
        } else {
            Reporter.log("No Car present", true);
        }
        webLibrary.moveToTop();
        Assert.assertTrue(result);
    }

    @Then("^Listing page url should contains below details$")
    public void listing_page_url_contains(List<String> list) throws InterruptedException {
        boolean result = false;
        String url = driver.getCurrentUrl();
        // converting all the element of list into lowercase
        List<String> res = list.stream()
                .map(String::toLowerCase)
                .collect(Collectors.toList());
        // replacing all the " " to - for url verification
        Collections.replaceAll(res, " ", "-");
        result = webLibrary.stringContainsItemFromList(url, res);
        Assert.assertTrue(result);
    }

    @Then("^Listing page filter should  be checked for below$")
    public void verifyChecked(List<String> list) {
        boolean result = false;
        List<WebElement> chekedItem = driver.findElements(By.cssSelector("input:checked[type='checkbox']"));
        if (chekedItem.size() == list.size()) {
            result = true;
        }
        //TODO write more cheecks for checkbox selected
        Assert.assertTrue(result);
    }

    @When("^User click on buy used car primary option \"([^\"]*)\"$")
    public void click_on_buy_used_car_option(String option) {
        String locator = null;
        if (option.contains("budget")) {
            locator = landingPageLocator.getProperty("byBudget");
        } else if (option.contains("owner")) {
            locator = landingPageLocator.getProperty("byOwner");
        } else if (option.contains("fuel")) {
            locator = landingPageLocator.getProperty("byFuel");
        } else if (option.contains("brand")) {
            locator = landingPageLocator.getProperty("byBrand");
        } else if (option.contains("body")) {
            locator = landingPageLocator.getProperty("byBodyType");
        } else {
            Reporter.log("Wrong option selected", true);
        }
        if (locator != null) {
            webLibrary.scrollToWebElement(locator);
            webLibrary.click(locator);
        }
    }

    @When("^User click on buy used car secondary option \"([^\"]*)\"$")
    public void user_click_on_secondary_option(String option) {
        String locator;
        if (option.contains("Owner")) {
            locator = "XP_//h5[contains(text()," + option.substring(0, 1) + ") and contains(text()," + option.substring(2, 4) + ") and contains(text()," + option.substring(6, option.length()) + ")]";
        } else if (option.contains("Lakh")) {
            locator = "XP_//div[contains(text(),'" + option + "')]";
        } else {
            locator = "XP_//h5[text()='" + option + "']";
        }
        Reporter.log(locator, true);
        webLibrary.scrollToWebElement(locator);
        webLibrary.click(locator);
    }

    @When("User click on the footer \"([^\"]*)\"")
    public void user_click_on_footer_option(String option) {
        webLibrary.scrollElement(driver);
        webLibrary.click("XP_//a[contains(text(),'" + option + "')]");
    }

    @When("Verify text on the footer \"([^\"]*)\" should be \"([^\"]*)\"")
    public void user_text_on_footer_option(String option, String text) {
        webLibrary.scrollElement(driver);
        Assert.assertEquals(webLibrary.getText("XP_//a[contains(text(),'" + option + "')]"), text);
    }

    @When("^Verify price range on landing page for \"([^\"]*)\"$")
    public void verifyBudgetInLandingPage(String range) {
        boolean result = true;
        List<WebElement> elements = driver.findElements(elementLocator.elements(landingPageLocator.getProperty("budgetLandingPage")));
        for (WebElement element : elements) {
            String text = element.getText();
            if (!text.equals(" ") && !text.equals("") && !text.equals(null)) {
                Reporter.log("tect" + text, true);
                webLibrary.click(landingPageLocator.getProperty("nextBttnCarousel"));
                result = webLibrary.verifyPriceRange(range, webLibrary.getPriceFromString(text));
                Reporter.log("report" + result, true);
                if (!result) {
                    break;
                }
            }
        }
        Assert.assertTrue(result);//webLibrary.getPriceFromWebElementList(range, elements));
    }

    @When("^When user click on view used cars under by budget$")
    public void viewAllCarUnderBuyBudget() {
        Reporter.log("inside click", true);
        webLibrary.click(landingPageLocator.getProperty("viewAllUsedCarBudget"));
    }

    @When("^User enters \"([^\"]*)\" with \"([^\"]*)\" on landing page$")
    public void userEnterCarOrModelInGlobalSearch(String item, String city) throws InterruptedException {
        Reporter.log(city, true);
        enterTextUsingAction(landingPageLocator.getProperty(item), city);
    }

    public void enterTextUsingAction(String element, String text) throws InterruptedException {
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(elementLocator.elements(element)));
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        Thread.sleep(500);
        actions.sendKeys(Keys.TAB);
        actions.build().perform();
    }
}
