#author : Kirti Gupta

# Reviewer: Sanjit Singh, Ankit Soni, Sarthak Shrivastava
#Prerequisites : should be log into df panel
@dealerOnBoarding
Feature: Dealer Onboarding
  As a user,
  I want to enroll dealer in DF panel

  Background:
    Given User is logged in on Unnati Panel

  @customerdetail
  Scenario:User enlisting a dealer in df panel
    Given User navigates to Customer Database
    When User navigates to Dealers grid
    And User clicks on Add Unnati Lite button
    And User clicks on save button of Customer Details Accordion
    And User verifies status becomes inProcess
    And User creates an offer for the dealer
    When User navigates to Enlisting form accordion
    When User enters text in all fields
    Then User clicks on save button and validates status changed to inProcess
    When User navigates to FI document accordion form, upload documents
    When User selects packages, status becomes Credit Agreement
    When User navigates to Enlisting Document Uplaod accordion, status becomes Enlistment Pending


