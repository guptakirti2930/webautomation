package screens.Lms;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author ishitachoudhary
 */

//finding and defining elements present on the page
public class LoginScreen {

    public LoginScreen(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "email")
    WebElement username;

    @FindBy(id = "password")
    WebElement password;

    @FindBy(id = "submit")
    WebElement loginbtn;

    @FindBy(linkText = "Home")
    WebElement home;

    public WebElement getUsername() {
        return username;
    }

    public WebElement getPassword() {
        return password;
    }

    public WebElement getLoginbtn() {
        return loginbtn;
    }

    public WebElement getHome() {
        return home;
    }
}
