package screens.Lms;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author ishitachoudhary
 */

//finding and defining elements present on the page
public class CreateLeadScreen {

    public CreateLeadScreen(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(linkText = "Home")
    WebElement homeMenu;

    @FindBy(linkText = "Create Lead")
    WebElement leadOption;

    @FindBy(xpath = "//div[2]/div/div/button")
    WebElement leadButton;

    @FindBy(id = "b_error_name")
    WebElement nameError;

    @FindBy(id = "b_error_phone")
    WebElement phoneError;

    @FindBy(id = "b_error_variant")
    WebElement variantError;

    @FindBy(id = "b_error_sourceAwareness")
    WebElement awarenessError;

    @FindBy(xpath = "//div[1]/div[1]/div/div[2]/input")
    WebElement customerName;

    @FindBy(xpath = "//div[2]/div/div[2]/input")
    WebElement customerPhone;

    @FindBy(xpath = "//div[2]/div/div/div[2]/input")
    WebElement customerEmail;

    @FindBy(id = "idMake")
    WebElement carMake;

    @FindBy(id = "idModel")
    WebElement carModel;

    @FindBy(id = "idYear")
    WebElement carYear;

    @FindBy(id = "idVariant")
    WebElement carVariant;

    @FindBy(id = "idOdometer")
    WebElement carOdometer;

    @FindBy(id = "idState")
    WebElement carState;

    @FindBy(id = "idCity")
    WebElement storeCity;

    @FindBy(id = "idBranchName")
    WebElement storeName;

    @FindBy(id = "idDateSlot")
    WebElement apptDate;

    @FindBy(id = "idTimeSlot")
    WebElement apptTime;

    @FindBy(id = "idSourceAwareness")
    WebElement sourceAwareness;

    @FindBy(xpath = "//div[8]/div[2]/div/div[2]/textarea")
    WebElement carReason;

    @FindBy(id = "addActivity")
    WebElement apptPopup;

    @FindBy(xpath = "//*[@id=\"addActivity\"]/div/button[1]")
    WebElement copyApptBtn;

    @FindBy(xpath = "//*[@id=\"addActivity\"]/div/button[2]")
    WebElement okayBtn;

    @FindBy(xpath = "//*[@id=\"addActivity\"]/h3")
    WebElement apptId;

    public WebElement getHomeMenu() {
        return homeMenu;
    }

    public WebElement getLeadOption() {
        return leadOption;
    }

    public WebElement getLeadButton() {
        return leadButton;
    }

    public WebElement getNameError() {
        return nameError;
    }

    public WebElement getPhoneError() {
        return phoneError;
    }

    public WebElement getVariantError() {
        return variantError;
    }

    public WebElement getAwarenessError() {
        return awarenessError;
    }

    public WebElement getCustomerName() {
        return customerName;
    }

    public WebElement getCustomerPhone() {
        return customerPhone;
    }

    public WebElement getCustomerEmail() {
        return customerEmail;
    }

    public WebElement getCarMake() {
        return carMake;
    }

    public WebElement getCarModel() {
        return carModel;
    }

    public WebElement getCarYear() {
        return carYear;
    }

    public WebElement getCarVariant() {
        return carVariant;
    }

    public WebElement getCarOdometer() {
        return carOdometer;
    }

    public WebElement getCarState() {
        return carState;
    }

    public WebElement getStoreCity() {
        return storeCity;
    }

    public WebElement getStoreName() {
        return storeName;
    }

    public WebElement getApptDate() {
        return apptDate;
    }

    public WebElement getApptTime() {
        return apptTime;
    }

    public WebElement getSourceAwareness() {
        return sourceAwareness;
    }

    public WebElement getCarReason() {
        return carReason;
    }

    public WebElement getApptPopup()
    {
        return apptPopup;
    }

    public WebElement getCopyApptBtn()
    {
        return copyApptBtn;
    }

    public WebElement getOkayBtn()
    {
        return okayBtn;
    }

    public WebElement getApptId() { return apptId; }
}
