package screens;

import jdk.nashorn.internal.objects.annotations.Function;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author kirtigupta
 */
public class DealeronboardingScreen {

    public DealeronboardingScreen(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "/html/body/div[1]/div[1]/div[3]/ul/li[2]/a")
    private WebElement customerDatabase;

    @FindBy(xpath = "//a[@href='/applicants/applicant']")
    private WebElement dealer;

    @FindBy(xpath = "//a[@href='/applicants/lite-applicant/view']")
    private WebElement addUnnatiLite;

    @FindBy(xpath = "//a[@href='#Customer-Details']/h4/strong")
    private WebElement dealerDetails;

    @FindBy(id = "applicantCode")
    private WebElement dealerId;

    @FindBy(xpath = "//*[@id=\"Customer-Details\"]/div/div/button")
    private WebElement fetchFromOms;

    @FindBy(name = "companyName")
    private WebElement dealershipName;

    @FindBy(name = "regionName")
    private WebElement regionName;

    @FindBy(id = "select2-regionName-11-container")
    private WebElement regiondropdown;

    @FindBy(name = "address")
    private WebElement address;

    @FindBy(name = "proprietor1")
    private WebElement prop1;

    @FindBy(id = "gender")
    private WebElement gender;

    @FindBy(name = "panProprietor1")
    private WebElement panProp1;

    @FindBy(name = "aadharProprietor1")
    private WebElement aadharProp1;

    @FindBy(id = "dateOfBirth")
    private WebElement dob;

    @FindBy(name = "entityConstitution")
    private WebElement entityConstitution;

    @FindBy(name = "businessOperationCategory")
    private WebElement businessOperationtype;

    @FindBy(name = "offerGenerateCibilScoreCheck")
    private WebElement generateCibilScoreCheck;


    @FindBy(id = "customer-detail-submit")
    private WebElement save;

    public WebElement getEnlistingForm() {
        return enlistingForm;
    }

    @FindBy(xpath = "//a[@href='#Empanelment-Form']/h4/strong")
    private WebElement enlistingForm;

    @FindBy(id = "proprietor2")
    private WebElement properietor2;

    @FindBy(name = "owner2Gender")
    private WebElement owner2Gender;

    @FindBy(id = "panProprietor2")
    private WebElement panProp2;

    @FindBy(name = "aadharProprietor2")
    private WebElement addharProp2;

    @FindBy(id = "proprietor3")
    private WebElement prop3;

    @FindBy(name = "owner3Gender")
    private WebElement owner3Gender;

    @FindBy(id = "panProprietor3")
    private WebElement panProp3;

    @FindBy(name = "aadharProprietor3")
    private WebElement aadharProp3;

    @FindBy(id = "proprietor4")
    private WebElement prop4;

    @FindBy(name = "owner4Gender")
    private WebElement owner4Gender;

    @FindBy(id = "panProprietor4")
    private WebElement panProp4;

    @FindBy(name = "aadharProprietor4")
    private WebElement aadharProp4;

    @FindBy(name = "email")
    private WebElement email;

    @FindBy(name = "primaryNumber")
    private WebElement phoneNo;

    @FindBy(name = "cutter")
    private WebElement cutter;

    @FindBy(name = "scrapper")
    private WebElement scrapper;

    @FindBy(id = "empanelmentFormSubmit")
    private WebElement enlistingSubmit;

    @FindBy(xpath = "//a[@href='#FI-Document-Upload']/h4/strong")
    private WebElement fiDoc;

    @FindBy(id = "1_comments")
    private WebElement pancardDocumentcomments;

    @FindBy(id = "1_file")
    private WebElement panCardDocument;

    public WebElement getEntityConstitution() {
        return entityConstitution;
    }


    public WebElement getAadharCardUpdatedBy() {
        return aadharCardUpdatedBy;
    }

    @FindBy(id = "2_updatedBy")
    private WebElement aadharCardUpdatedBy;

    @FindBy(xpath = "//*[@id=\"fi-document-table\"]/tbody/tr[1]/td[4]/span[2]/a")
    private WebElement uploadPanCard;

    @FindBy(id = "2_comments")
    private WebElement aadharCardComments;

    @FindBy(id = "2_file")
    private WebElement aadharCardDocument;

    public WebElement getDealerPicComments() {
        return dealerPicComments;
    }

    @FindBy(id="3_comments")
    private WebElement dealerPicComments;

    public WebElement getDealerPic() {
        return dealerPic;
    }

    @FindBy(id="3_file")
    private WebElement dealerPic;

    public WebElement getDealerPicUpload() {
        return dealerPicUpload;
    }

    @FindBy(xpath="//*[@id=\"fi-document-table\"]/tbody/tr[3]/td[4]/span[2]/a")
    private WebElement dealerPicUpload;


    public WebElement getCoapplicantComments() {
        return coapplicantComments;
    }

    @FindBy(id="4_comments")
    private WebElement coapplicantComments;

    public WebElement getCoapplicantDoc() {
        return coapplicantDoc;
    }

    @FindBy(id="4_file")
    private WebElement coapplicantDoc;

    public WebElement getCoapplicantUpload() {
        return coapplicantUpload;
    }

    @FindBy(xpath="//*[@id=\"fi-document-table\"]/tbody/tr[4]/td[4]/span[2]/a")
    private WebElement coapplicantUpload;

    @FindBy(xpath = "//*[@id=\"fi-document-table\"]/tbody/tr[2]/td[4]/span[2]/a")
    private WebElement uploadAadharCard;

    public WebElement getPackageAccordion() {
        return packageAccordion;
    }

    public WebElement getPackageSelection() {
        return packageSelection;
    }


    @FindBy(xpath = "//div[@class='col-md-12 card-content']//div[3]//div[1]//a[1]//h4[1]")
    private WebElement packageAccordion;


    @FindBy(id = "packageNameSelection")
    private WebElement packageSelection;

    public WebElement getPackageSubmit() {
        return packageSubmit;
    }

    @FindBy(id = "packageSelectionFormSubmit")
    private WebElement packageSubmit;


    public WebElement getEnlistingDocumentUpload() {
        return enlistingDocumentUpload;
    }

    @FindBy(xpath = "//a[@href=\"#Document-Upload\"]/h4")
    public WebElement enlistingDocumentUpload;

    public WebElement getSignedEnlistingForm() {
        return signedEnlistingForm;
    }

    @FindBy(id = "empanelmentFormPath")
    public WebElement signedEnlistingForm;


    public WebElement getCreditAgreement() {
        return creditAgreement;
    }

    @FindBy(id = "creditAgreementPath")
    public WebElement creditAgreement;

    public WebElement getBlankCheque() {
        return blankCheque;
    }

    @FindBy(id = "blankChequePath")
    public WebElement blankCheque;

    public WebElement getEnlistingDocumentSubmit() {
        return enlistingDocumentSubmit;
    }

    @FindBy(xpath="//*[@id=\"Document-Upload\"]/div/div/div/div[2]/button")
    private WebElement enlistingDocumentSubmit;

    public WebElement getPackageSelectionMessage() {
        return packageSelectionMessage;
    }

    @FindBy(xpath = "/html/body/div[1]/div[2]/div[1]/ul/li")
    private WebElement packageSelectionMessage;

    public WebElement getNachMandatory() {
        return nachMandatory;
    }

    @FindBy(xpath = "//*[@id=\"Document-Upload\"]/div/div/div")
    public WebElement nachMandatory;


    public WebElement getDealerConfig() {
        return dealerConfig;
    }

    @FindBy(xpath = "//*[@id=\"applicant-menu\"]/ul/li[3]/a")
    private WebElement dealerConfig;

    public WebElement getDealerCodeColumn() {
        return dealerCodeColumn;
    }

    //@FindBy(xpath = "//*[@id=\"1555672477417-body-grid-container\"]/div[1]/div/div/div/div/div/div[2]/div/div[3]/div/div/input")
    //@FindBy(xpath = "//*[@id=\"1555507912785-body-grid-container\"]/div[1]/div/div/div/div/div/div[2]/div/div[3]/div/div/input")
    @FindBy(xpath="//*/div[1]/div/div/div/div/div/div[2]/div/div[3]/div/div/input")
    private WebElement dealerCodeColumn;

    public WebElement getDealerConfigViewClick() {
        return dealerConfigViewClick;
    }

    @FindBy(xpath = "//*[@id]/div/a")
    private WebElement dealerConfigViewClick;

    public WebElement getNachAccordion() {
        return nachAccordion;
    }

    @FindBy(xpath = "//a[contains(@href,\"#nach-required\")]/h4")
    private WebElement nachAccordion;

    public WebElement getIsNachMandatory() {
        return isNachMandatory;
    }

    @FindBy(id = "nachRequired")
    private WebElement isNachMandatory;

    public WebElement getNachUpdate() {
        return nachUpdate;
    }

    @FindBy(id = "nachRequiredUpdate")
    private WebElement nachUpdate;


    public WebElement getDealerConfigurationPage() {
        return dealerConfigurationPage;
    }

    @FindBy(xpath="/html/body/div[1]/div[2]/div[1]//div/div[2]/ol/h5")
    private WebElement dealerConfigurationPage;

    public WebElement getDealerCodeColumnDealerGrid() {
        return dealerCodeColumnDealerGrid;
    }

    @FindBy(xpath="/html/body/div[1]/div[2]/div[1]/div/div/div[2]/form/table/thead/tr[2]/td[3]/div/input")
    private WebElement dealerCodeColumnDealerGrid;

    public WebElement getDealerCodeLink() {
        return dealerCodeLink;
    }

    @FindBy(xpath="//*[@id]/td[3]/a")
    private WebElement dealerCodeLink;

    public WebElement getProperietor2() {
        return properietor2;
    }

    public WebElement getOwner2Gender() {
        return owner2Gender;
    }

    public WebElement getPanProp2() {
        return panProp2;
    }

    public WebElement getAddharProp2() {
        return addharProp2;
    }

    public WebElement getProp3() {
        return prop3;
    }

    public WebElement getOwner3Gender() {
        return owner3Gender;
    }

    public WebElement getPanProp3() {
        return panProp3;
    }

    public WebElement getAadharProp3() {
        return aadharProp3;
    }

    public WebElement getProp4() {
        return prop4;
    }

    public WebElement getOwner4Gender() {
        return owner4Gender;
    }

    public WebElement getPanProp4() {
        return panProp4;
    }

    public WebElement getAadharProp4() {
        return aadharProp4;
    }

    public WebElement getEmail() {
        return email;
    }

    public WebElement getPhoneNo() {
        return phoneNo;
    }

    public WebElement getCutter() {
        return cutter;
    }

    public WebElement getScrapper() {
        return scrapper;
    }


    public WebElement getEnlistingSubmit() {
        return enlistingSubmit;
    }

    public WebElement getFiDoc() {
        return fiDoc;
    }

    public WebElement getPancardDocumentcomments() {
        return pancardDocumentcomments;
    }

    public WebElement getPanCardDocument() {
        return panCardDocument;
    }

    public WebElement getAadharCardComments() {
        return aadharCardComments;
    }

    public WebElement getAadharCardDocument() {
        return aadharCardDocument;
    }

    public WebElement getUploadPanCard() {
        return uploadPanCard;
    }

    public WebElement getUploadAadharCard() {
        return uploadAadharCard;
    }


    public WebElement getCustomerDatabase() {
        return customerDatabase;
    }

    public WebElement getDealer() {
        return dealer;
    }

    public WebElement getAddUnnatiLite() {
        return addUnnatiLite;
    }

    public WebElement getDealerDetails() {
        return dealerDetails;
    }

    public WebElement getDealerId() {
        return dealerId;
    }

    public WebElement getFetchFromOms() {
        return fetchFromOms;
    }

    public WebElement getDealershipName() {
        return dealershipName;
    }

    public WebElement getRegionName() {
        return regionName;
    }

    public WebElement getRegiondropdown() {
        return regiondropdown;
    }

    public WebElement getAddress() {
        return address;
    }

    public WebElement getProp1() {
        return prop1;
    }

    public WebElement getGender() {
        return gender;
    }

    public WebElement getPanProp1() {
        return panProp1;
    }

    public WebElement getAadharProp1() {
        return aadharProp1;
    }

    public WebElement getDob() {
        return dob;
    }

    public WebElement getBusinessOperationtype() {
        return businessOperationtype;
    }

    public WebElement getGenerateCibilScoreCheck() {
        return generateCibilScoreCheck;
    }

    public WebElement getSave() {
        return save;
    }
}
