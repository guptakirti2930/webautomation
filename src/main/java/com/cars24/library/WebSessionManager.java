package com.cars24.library;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author kirtigupta
 */
public class WebSessionManager {
    private static WebSessionManager instance = null;  // instance variable for WebsessionManager
    public WebDriver driver = null;
    static String osInfo = System.getProperty("os.name");
    static String workingDirectory = System.getProperty("user.dir");
    static String dataDirectoryPath = workingDirectory + File.separator + "data" + File.separator + "data.properties";
    static String dealerDataDirectoryPath = workingDirectory + File.separator + "data" + File.separator + "DF-DealerOnboarding.properties";
    public static Properties dataProp = propertyFile(dataDirectoryPath);
    public static Properties dealerProp = propertyFile(dealerDataDirectoryPath);


    static String browsr = System.getProperty("browser");
    public static String browsername = (browsr != null) ? browsr : dataProp.getProperty("browser");

    static String url = System.getProperty("url");
    public static String inputurl = (url != null) ? url : dataProp.getProperty("url");


    //getting instance of WebSessionManger
    public static WebSessionManager getInstance() {
        if (instance == null) {
            instance = new WebSessionManager();
        }
        return instance;
    }

    //Initiating WebDriverInstance
    public void initiateWebDriverInstance(String browserName) throws InterruptedException {
        if (browserName.equalsIgnoreCase("chrome")) {
            driver = startChromeBrowser();
        } else if (browserName.equalsIgnoreCase("firefox")) {
            driver = startFirefoxBrowser();
        }
        driver.manage().window().maximize();
        //to open url in the browser
        driver.get(inputurl);
    }

    public String getChromeDriverPath() {
        String path = null;
        try {
            if (osInfo.toLowerCase().contains("window")) {
                path = workingDirectory + File.separator + "lib" + File.separator + "win" + File.separator + "chromedriver.exe";
            } else {
                path = workingDirectory + File.separator + "lib" + File.separator + "linux" + File.separator + "chromedriver";
            }
        } catch (Exception e) {
        }
        return path;
    }


    public WebDriver startChromeBrowser() throws InterruptedException {
        try {
            WebDriver driver = null;
            String path = getChromeDriverPath();
            System.setProperty("webdriver.chrome.driver", path);
            ChromeOptions options = new ChromeOptions();
            options.addArguments("disable-infobars");
            driver = new ChromeDriver(options);
            return driver;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getFirefoxDriverPath() {
        String path = null;
        try {
            if (osInfo.toLowerCase().contains("window")) {
                path = workingDirectory + File.separator + "lib" + File.separator + "win" + File.separator + "geckodriver.exe";
            } else {
                path = workingDirectory + File.separator + "lib" + File.separator + "linux" + File.separator + "geckodriver";
            }
        } catch (Exception e) {
        }
        return path;
    }

    public WebDriver startFirefoxBrowser() throws InterruptedException {
        try {
            WebDriver driver = null;
            String path = getFirefoxDriverPath();
            System.setProperty("webdriver.gecko.driver", path);
            driver = new FirefoxDriver();
            return driver;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Properties propertyFile(String filename) {
        Properties temp = new Properties();
        FileInputStream fileInput = null;
        try {
            fileInput = new FileInputStream(filename);
            temp.load(fileInput);
        } catch (Exception e) {

        } finally {
            try {
                if (fileInput != null) {
                    fileInput.close();
                }
            } catch (IOException e) {
            }
        }
        return temp;
    }

}
