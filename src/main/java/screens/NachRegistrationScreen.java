package screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NachRegistrationScreen {
    public NachRegistrationScreen(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }


    @FindBy(xpath = "//*[text()='eNach Management']")
    private WebElement enachMenu;

    @FindBy(xpath = "//a[@href=\"/applicants/enach/index\"]")
    private WebElement registrationMenu;

    @FindBy(xpath = "//a[@href=\"/applicants/paper-nach/view\"]")
    private WebElement paperNachButton;

    @FindBy(xpath = "//a[@href=\"#paperNachRegistration\"]/h4")
    private WebElement paperNachDetailsAccordion;

    @FindBy(xpath = "  //*[@id=\"paperNachRegistrationForm\"]/div[1]/div[1]/div/select[@id=\"applicantBankId\"]")
    private WebElement selectDealerBank;

    @FindBy(xpath = "//*[@id=\"paperNachRegistrationForm\"]/div[1]/div[2]/input[@id=\"paperNachImagePath\"]")
    private WebElement uploadDocument;

    @FindBy(id = "paperNachFormSubmit")
    private WebElement paperNachDetailsSubmit;

    @FindBy(xpath = "//a[@href=\"#paperNachVerifcation\"]/h4")
    private WebElement verificationPaperNach;

    @FindBy(xpath = "//*[@id=\"paperNachVerifcation\"]/form/div[2]/div/div/a")
    private WebElement paperNachVerifiationDoc;

    @FindBy(xpath = "//*[@id=\"paperNachVerifcation\"]/form/div[3]/div/a[1]")
    private WebElement verificationPaperNachSubmit;

    @FindBy(xpath = "//*[@id=\"popupSubmitMessage-title\"]")
    private WebElement verificationConfirmationPopUpBox;

    @FindBy(id = "popupSubmitId")
    private WebElement popupSubmit;

    @FindBy(xpath = "//a[@href=\"#paperNachDispatch\"]/h4")
    private WebElement dispatchDetails;

    @FindBy(xpath = "//*[@id=\"dispachDate\"]")
    private WebElement dispatchDate;

    @FindBy(xpath = "//*[@id=\"courierName\"]")
    private WebElement courierName;

    @FindBy(xpath = "//*[@id=\"trackingId\"]")
    private WebElement trackingId;

    @FindBy(xpath = "//*[@id=\"paperNachDispatchForm\"]/div[2]/div/a[1]")
    private WebElement dispatchSubmit;

    @FindBy(xpath = "//*[@id=\"popupSubmitMessage-modal\"]/div/div/div[2]")
    private WebElement popupBox;

    @FindBy(xpath = "//*[@id=\"popupSubmitId\"]")
    private WebElement dispatchdetailPopupSubmit;

    @FindBy(xpath = "//a[@href=\"#paperNachDocumentReceived\"]/h4")
    private WebElement documentReceivedDetails;

    @FindBy(xpath = "//*[@id=\"documentReceivedDate\"]")
    private WebElement documentReceivedDate;

    @FindBy(xpath = "//*[@id=\"paperNachDocumentReceivedForm\"]/div[2]/div/a[1]")
    private WebElement documentReceivedSubmit;

    @FindBy(xpath = "//*[@id=\"popupSubmitMessage-modal\"]/div/div/div[2]")
    private WebElement documentPopup;

    @FindBy(id = "popupSubmitId")
    private WebElement documentPopupSubmit;

    @FindBy(xpath = "//a[@href=\"#paperNachSentToBank\"]/h4")
    private WebElement bankDetails;

    @FindBy(xpath = "//*[@id=\"maximumAmount\"]")
    private WebElement maxAmount;

    @FindBy(xpath = "//*[@id=\"sentToBankImagePath\"]")
    private WebElement documentSendToBank;

    @FindBy(xpath = "//*[@id=\"paperNachSentToBankForm\"]/div[2]/div/a[1]")
    private WebElement sendToBankSubmitButton;

    @FindBy(xpath = "//*[@id=\"popupSubmitMessage\"]")
    private WebElement sendToBankDetailsPopup;

    @FindBy(xpath = "//*[@id=\"popupSubmitId\"]")
    private WebElement sendToBankPopupSubmit;

    @FindBy(xpath = "//a[@href=\"#paperNachBankApprove\"]/h4")
    private WebElement bankApproval;

    @FindBy(xpath = "//*[@id=\"originalMandateId\"]")
    private WebElement UMRN;

    @FindBy(xpath = "//*[@id=\"bankRecieveDate\"]")
    private WebElement bankReceivedDate;

    @FindBy(xpath = "//*[@id=\"paperNachBankApprovalForm\"]/div[2]/div/a[1]")
    private WebElement bankApprovalSubmit;

    @FindBy(xpath = "//*[@id=\"popupSubmitMessage\"]")
    private WebElement bankApprovalPopup;

    @FindBy(xpath = "//*[@id=\"popupSubmitId\"]")
    private WebElement bankApprovalPopupSubmit;

    @FindBy(xpath="/html/body/div[1]/div[2]/div[1]/div/div/div/div[3]/h4/span")
    private WebElement status;

    public WebElement getEnachMenu() {
        return enachMenu;
    }

    public WebElement getRegistrationMenu() {
        return registrationMenu;
    }

    public WebElement getPaperNachButton() {
        return paperNachButton;
    }

    public WebElement getPaperNachDetailsAccordion() {
        return paperNachDetailsAccordion;
    }

    public WebElement getSelectDealerBank() {
        return selectDealerBank;
    }

    public WebElement getUploadDocument() {
        return uploadDocument;
    }

    public WebElement getPaperNachDetailsSubmit() {
        return paperNachDetailsSubmit;
    }

    public WebElement getVerificationPaperNach() {
        return verificationPaperNach;
    }

    public WebElement getPaperNachVerifiationDoc() {
        return paperNachVerifiationDoc;
    }

    public WebElement getVerificationPaperNachSubmit() {
        return verificationPaperNachSubmit;
    }

    public WebElement getVerificationConfirmationPopUpBox() {
        return verificationConfirmationPopUpBox;
    }

    public WebElement getPopupSubmit() {
        return popupSubmit;
    }


    public WebElement getDispatchDetails() {
        return dispatchDetails;
    }

    public WebElement getDispatchDate() {
        return dispatchDate;
    }

    public WebElement getCourierName() {
        return courierName;
    }

    public WebElement getTrackingId() {
        return trackingId;
    }

    public WebElement getDispatchSubmit() {
        return dispatchSubmit;
    }

    public WebElement getPopupBox() {
        return popupBox;
    }

    public WebElement getDispatchdetailPopupSubmit() {
        return dispatchdetailPopupSubmit;
    }

    public WebElement getDocumentReceivedDetails() {
        return documentReceivedDetails;
    }

    public WebElement getDocumentReceivedDate() {
        return documentReceivedDate;
    }

    public WebElement getDocumentReceivedSubmit() {
        return documentReceivedSubmit;
    }


    public WebElement getDocumentPopup() {
        return documentPopup;
    }

    public WebElement getDocumentPopupSubmit() {
        return documentPopupSubmit;
    }

    public WebElement getBankDetails() {
        return bankDetails;
    }

    public WebElement getMaxAmount() {
        return maxAmount;
    }

    public WebElement getDocumentSendToBank() {
        return documentSendToBank;
    }

    public WebElement getSendToBankSubmitButton() {
        return sendToBankSubmitButton;
    }

    public WebElement getSendToBankDetailsPopup() {
        return sendToBankDetailsPopup;
    }

    public WebElement getSendToBankPopupSubmit() {
        return sendToBankPopupSubmit;
    }

    public WebElement getBankApproval() {
        return bankApproval;
    }

    public WebElement getUMRN() {
        return UMRN;
    }

    public WebElement getBankReceivedDate() {
        return bankReceivedDate;
    }

    public WebElement getBankApprovalSubmit() {
        return bankApprovalSubmit;
    }

    public WebElement getBankApprovalPopup() {
        return bankApprovalPopup;
    }

    public WebElement getBankApprovalPopupSubmit() {
        return bankApprovalPopupSubmit;
    }

    public WebElement getStatus() {
        return status;
    }
}


