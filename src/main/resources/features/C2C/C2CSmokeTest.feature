#author : Sarthak Srivastava
# Reviewer: Sanjit Singh, Ankit Soni, Kapil Garg

@C2C
Feature: C2C smoke test functionality
  As a user,
  I want to do smoke test for C2C feature of CARS24

  #Currently this test is not working for Delhi-NCR as city
  @c2c-smoke
  Scenario Outline: User want to search filter from Landing page
    Given User open c2c website
    And User enters city "<city>" on landing page
    And User enters model "<model>" on landing page
    And User enters budget "<budget>" on landing page
    When User click on search
    Then Verify by city "<city>" on listing page
    And Verify Bubble selected in listing page
      | <model>  |
      | <budget> |
    And Listing page should contains car with below filter
      | <city>  |
      | <model> |
    And Listing page url should contains below details
      | <city>  |
      | <model> |
    And Listing page filter should  be checked for below
      | <model>  |
      | <budget> |
    Examples:
      | city      | model   | budget         |
      | Gurgaon   | Maruti  | Under ₹2 Lakhs |
      | Ahmedabad | Hyundai | Under ₹2 Lakhs |
      | Ahmedabad | Maruti  | ₹5 - 10 Lakhs  |
      | Faridabad | Maruti  | Under ₹2 Lakhs |
      | Ghaziabad | Maruti  | Under ₹2 Lakhs |


  @c2c-smoke
  Scenario Outline: User want to test Buy Used Cars options with buy budget
    Given User open c2c website
    When User click on buy used car secondary option "<budget>"
  #  Then Verify price range on landing page for "<budget>" //TODO need to fix carousel
    And When user click on view used cars under by budget
    Then Verify Bubble selected in listing page
      | <budget> |
    And Listing page filter should  be checked for below
      | <budget> |
    And Verify price range selected on cars displayed "<budget>"
    Examples:
      | budget          |
      | Under ₹2 Lakhs  |
      | Above ₹10 Lakhs |
      | ₹2 - 5 Lakhs    |
      | ₹5 - 10 Lakhs   |

  @c2c-smoke
  Scenario Outline: User want to test Buy Used Cars options with buy fuel type
    Given User open c2c website
    And  User click on buy used car primary option "by fuel type"
    When User click on buy used car secondary option "<fuel type>"
    Then Listing page selected checkbox should be "<fuel type>"
    And Verify Bubble selected in listing page
      | <fuel type> |
    Examples:
      | fuel type |
      | Petrol    |
      | Diesel    |
      | CNG       |
      | Hybrid    |

  @c2c-smoke
  Scenario Outline: User want to test Buy Used Cars options with by owner
    Given User open c2c website
    And  User click on buy used car primary option "by owner"
    When User click on buy used car secondary option "<owner>"
    Then Verify Bubble selected in listing page
      | <owner> |
    And  Listing page filter should  be checked for below
      | <owner> |
    Examples:
      | owner             |
      | 1st Owner         |
      | 2nd Owner         |
      | 3rd Owner         |
      | 4th Owner & above |

  @c2c-smoke
  Scenario Outline: Verify footer links By Brand are working fine or not for
    Given User open c2c website
    And User enters city "<city>" on landing page
    When Verify text on the footer "<footer>" should be "<text>"
    And User click on the footer "<footer>"
    Then Listing page should contains car with below filter
      | <footer> |
    And Listing page url should contains below details
      | <footer> |
      | <city>   |
    Examples:
      | footer        | text               | city      |
      | Maruti Suzuki | Used Maruti Suzuki | Noida     |
      | Honda         | Used Honda         | Noida     |
      | Tata          | Used Tata          | Gurgaon   |
      | Toyota        | Used Toyota        | Gurgaon   |
      | BMW           | Used BMW           | New Delhi |
      | Chevrolet     | Used Chevrolet     | New Delhi |
      | Ford          | Used Ford          | Faridabad |
      | Mahindra      | Used Mahindra      | Faridabad |
      | Volkswagen    | Used Volkswagen    | Gurgaon   |
      | Renault       | Used Renault       | Noida     |
      | Datsun        | Used Datsun        | Noida     |

  @c2c-smoke
  Scenario Outline: Verify footer links By Model are working fine or not for
    Given User open c2c website
    When Verify text on the footer "<footer>" should be "<text>"
    And User click on the footer "<footer>"
    Then Listing page should contains car with below filter
      | <footer> |
    And Listing page url should contains below details
      | <footer> |
    Examples:
      | footer      | text             |
      | Baleno      | Used Baleno      |
      | Alto K10    | Used Alto K10    |
      | Swift Dzire | Used Swift Dzire |
      | Swift       | Used Swift       |
      | Honda City  | Used Honda City  |
      | Elite i20   | Used Elite i20   |
      | Grand i10   | Used Grand i10   |
      | Brezza      | Used Brezza      |
      | Wagon R     | Used Wagon R     |
      | Creta       | Used Creta       |
      | Redi Go     | Used Redi Go     |
      | Verna       | Used Verna       |

  @c2c-smoke
  Scenario Outline: Verify footer links By Budget are working fine or not for
    Given User open c2c website
    And User enters city "<city>" on landing page
    When User click on the footer "<footer>"
    Then Verify Bubble selected in listing page
      | <footer> |
    And Verify price range selected on cars displayed "<footer>"
    And Listing page filter should  be checked for below
      | <city> |
    Examples:
      | footer          | city      |
      | Under ₹2 Lakhs  | Ahmedabad |
      | ₹2 - 5 Lakhs    | Noida     |
      | ₹5 - 10 Lakhs   | Gurgaon   |
      | Above ₹10 Lakhs | New delhi |


  @c2c-smoke
  Scenario Outline: User want to test Buy Used Cars options with by body type
    Given User open c2c website
    And  User click on buy used car primary option "by body type"
    When User click on buy used car secondary option "<bodyType>"
    Then Verify Bubble selected in listing page
      | <bodyType> |
    And  Listing page filter should  be checked for below
      | <bodyType> |
    And Listing page url should contains below details
      | <bodyType> |
    Examples:
      | bodyType     |
      | SUV          |
      | Hatchback    |
      | Sedan        |
      | Luxury Sedan |
      | Luxury SUV   |

  @auto   @c2c-smoke
  Scenario Outline: User want to test global search filter on Landing page
    Given User open c2c website
    And User enters city "<city>" on landing page
    When User enters "globalSearch" with "<model>" on landing page
    Then Verify Bubble selected in listing page
      | <model> |
    And Listing page should contains car with below filter
      | <city>  |
      | <model> |
    And Listing page url should contains below details
      | <city>  |
      | <model> |
    And Listing page filter should  be checked for below
      | <model> |
      | <city>  |
    Examples:
      | city      | model   |
      | Gurgaon   | Hyundai |
      | Noida     | Maruti  |
      | New Delhi | Scorpio |
      | Gurgaon   | Hyundai |