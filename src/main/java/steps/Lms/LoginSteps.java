package steps.Lms;

import com.cars24.constants.Constants;
import com.cars24.library.WebLibrary;
import com.cars24.library.WebSessionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;
import screens.Lms.LoginScreen;
import sun.misc.BASE64Decoder;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author ishitachoudhary
 */

// mapping of feature file with gherkin syntax and calling methods of screen and weblibrary class.
public class LoginSteps extends WebSessionManager {

    private WebDriver driver = WebSessionManager.getInstance().driver;
    private LoginScreen login_screen = new LoginScreen(driver);
    private File login = new File("loginFolder/" + ".txt");
    private WebLibrary webLibrary = new WebLibrary(driver);
    String key = "lockUnlock";

    @Given("^User is on login page$")
    public void user_login_page() {
        verifyLoginScreenElements();
        Reporter.log("user is on login page", true);
    }

    @When("^User enters (.+) credentials of (.+)$")
    public void enter_credentials(String flag, String type) {
        Reporter.log("Enter login crdentials", true);
        enterLoginCredentials(flag, type);
    }

    @And("^User clicks on login button$")
    public void click_login_btn() {
        webLibrary.clickonWebElement(login_screen.getLoginbtn());
        Reporter.log("user clicks on login button now", true);
    }

    @Then("^User is (.+) to login")
    public void verifyLogin(String state) {
        if (state.equalsIgnoreCase("successful")) {
            Reporter.log("Verifying Successful Login", true);
            Assert.assertTrue(SuccessfulLogin());
        } else {
            Reporter.log("Verifying Unsuccessful Login");
            Assert.assertTrue(unSuccessfulLogin());
        }
    }

    //sending username and password values from constants class.
    public void enterLoginCredentials(String flag, String type) {
        if ("internal user".equalsIgnoreCase(type)) {
            if (flag.equalsIgnoreCase("valid")) {
                String internalPwd = decryptXOR(Constants.int_password, key);
                login_screen.getUsername().sendKeys(Constants.internal_user);
                login_screen.getPassword().sendKeys(internalPwd);
            } else {
                String invalidPwd = decryptXOR(Constants.invalid_password, key);
                login_screen.getUsername().sendKeys(Constants.invalid_username);
                login_screen.getPassword().sendKeys(invalidPwd);
            }
        } else {
            if (flag.equalsIgnoreCase("valid")) {
                String externalPwd = decryptXOR(Constants.ext_password, key);
                login_screen.getUsername().sendKeys(Constants.external_user);
                login_screen.getPassword().sendKeys(externalPwd);
            } else {
                String invalidPwd = decryptXOR(Constants.invalid_password, key);
                login_screen.getUsername().sendKeys(Constants.invalid_username);
                login_screen.getPassword().sendKeys(invalidPwd);
            }
        }
    }

    private void verifyLoginScreenElements() {
        webLibrary.waitforWebElement(login_screen.getUsername(), Integer.parseInt(dataProp.getProperty("DEFAULT_OBJECT_WAIT_TIME")));
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(login_screen.getUsername()));
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(login_screen.getPassword()));
        Assert.assertTrue(webLibrary.isElementPresentOnScreen(login_screen.getLoginbtn()));
    }

    public boolean unSuccessfulLogin() {
        if (webLibrary.isElementPresentOnScreen(login_screen.getUsername())) {
            Reporter.log("User is not logged in", true);
            return true;
        }
        else {
            Reporter.log("User is logged in");
            return false;
        }
    }

    public boolean SuccessfulLogin() {
        if (webLibrary.isElementPresentOnScreen(login_screen.getHome())) {
            Reporter.log("Sign up SuccessFull", true);
            createLoginFolder();
            return true;
        } else
        return false;
    }

    public static String decryptXOR(String message, String key) {
        try {
            if (message == null || key == null) return null;
            BASE64Decoder decoder = new BASE64Decoder();
            char[] keys = key.toCharArray();
            char[] mesg = new String(decoder.decodeBuffer(message)).toCharArray();
            int ml = mesg.length;
            int kl = keys.length;
            char[] newmsg = new char[ml];
            for (int i = 0; i < ml; i++) {
                newmsg[i] = (char) (mesg[i] ^ keys[i % kl]);
            }
            mesg = null;
            keys = null;
            return new String(newmsg);
        } catch (Exception e) {
            return null;
        }
    }

    private void createLoginFolder() {
        Reporter.log("Creating login folder", true);
        String loginState = "login = 1";
        loginState("login.txt", loginState);
    }

    private void loginState(String fileName, String loginState) {
        PrintWriter logFileWriter = null;
        try {
            FileUtils.forceMkdir(new File("loginFolder"));
            File logFile = new File("loginFolder/" + fileName);
            logFileWriter = new PrintWriter(logFile);
            logFileWriter.println(loginState);
            logFileWriter.flush();
        } catch (IOException e) {
            Reporter.log("Could not write log file\n" + ExceptionUtils.getStackTrace(e), true);
        } catch (Exception e) {
            Reporter.log("Exception while capturing device logs\n" + ExceptionUtils.getStackTrace(e),
                    true);
        } finally {
            if (logFileWriter != null) {
                logFileWriter.close();
            }
        }
    }

}
