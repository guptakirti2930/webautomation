package steps.C2C;

import com.cars24.library.C2CWebLibrary;
import com.cars24.library.ElementLocator;
import com.cars24.library.WebSessionManager;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class C2CListingStep extends WebSessionManager {

    private WebDriver driver = WebSessionManager.getInstance().driver;
    private C2CWebLibrary webLibrary = new C2CWebLibrary(driver);
    String env = System.getProperty("env");
    String inputEnvUrl = (env != null) ? dataProp.getProperty("env") : dataProp.getProperty("c2c_qa");
    String path = System.getProperty("user.dir") + File.separator + "data" + File.separator
            + "C2C-ListingPage.properties";
    Properties listingPageLocator = propertyFile(path);
    ElementLocator elementLocator = new ElementLocator();

    @Then("^Verify by city \"([^\"]*)\" on listing page$")
    public void verifyByCity(String city) {
        Assert.assertEquals(city, webLibrary.getText(listingPageLocator.getProperty("byCity")));
    }

    @Then("^Verify Bubble selected in listing page$")
    public void list_of_car_contains_city_model_and_budget(List<String> list) throws InterruptedException {
        boolean result = false;
        Thread.sleep(500);
        String bubbleText = webLibrary.getText(listingPageLocator.getProperty("bubble"));
        result = webLibrary.stringContainsItemFromList(bubbleText, list);
        Assert.assertTrue(result);
    }

    @Then("^Listing page selected checkbox should be \"([^\"]*)\"$")
    public void verify_filter_selected(String content) throws InterruptedException {
        boolean result = false;
        Thread.sleep(500);
        webLibrary.scrollToWebElement(listingPageLocator.getProperty("checkbox"));
        WebElement chekedItem = driver.findElement(By.cssSelector("input:checked[type='checkbox']"));
        if (content.equals("Under ₹2 Lakhs")){
            content = "0 - 2 Lakhs";
        }
        if(chekedItem.getAttribute("outerHTML").toLowerCase().contains(content.replaceAll("₹","").toLowerCase())){
            result = true;
        }
        Assert.assertTrue(result);    }

    @When("^Verify price range selected on cars displayed \"([^\"]*)\"$")
    public void verify_price_range(String range) {
        webLibrary.scrollElement(driver);
        webLibrary.moveToTop();
        List<WebElement> carList = webLibrary.getWebelementsList(listingPageLocator.getProperty("carsList"));
        Assert.assertTrue(webLibrary.getPriceFromWebElementList(range, carList));
    }

}
